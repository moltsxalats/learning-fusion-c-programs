/* PRograma en C per experimentar amb els patrons i la posició dels sprites.
 He utilitzat el primer món del Usas per veure com ho feien. Usen patrons de 8x8 pel que he pogut deduir la imatge.
Començarem carregant la imatge a VRAM*/

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include <string.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); // Skip 0x29 first bytes of the file. És un binari tal i com diu el manual. Els 7 primers indiquen la posició de la memòria i els 22 següents és el codi
  /*
C000  ld     b,#10        06 10
C002  ld     d,#00        16 00
C004  ld     hl,#c022     21 22 C0  ;; És on comencen els bytes de la paleta
C007  ld     a,(hl)       7E
C008  sla    a            CB 27
C00A  sla    a            CB 27
C00C  sla    a            CB 27
C00E  sla    a            CB 27
C010  ld     c,a          4F
C011  inc    hl           23
C012  ld     e,(hl)       5E
C013  inc    hl           23
C014  ld     a,(hl)       7E
C015  add    a,c          81
C016  ld     ix,#014d     DD 21 4D 01
C01A  call   #015f        CD 5F 01   // Crida a una rutina de subrom http://map.tni.nl/resources/msxbios.php. La carregada a ix, en aquest 014d que segons la mateixa pàgina indica imprimir per impressora ¿¿???
C01D  inc    d            14
C01E  inc hl           23
C01F  djnz   #c007        10 E6
C021  ret                 C9
*/
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("USASVIEW.s05", 0, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette_MSXViewer("USASVIEW.pal", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);
}

void main(){
  Screen(5);
  init_pantalla_joc();
  WaitForKey();
  Screen(0);
  RestorePalette();
  Exit(0);
}
