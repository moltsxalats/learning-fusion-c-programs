#include "../fusion-c/include/msx_fusion.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define HALT __asm halt __endasm // wait for the next interrupt

#define TAMANY_CAP_FUI 40
#define MS_WAIT while ((MS_FM_BASE_STATUS_REG1) & 0x01) {}
static FCB file;

// Registres del Moonsound
__sfr __at 0xC4 MS_FM_BASE_STATUS_REG1;
__sfr __at 0xC5 MS_FM_DATA1;
__sfr __at 0xC6 MS_FM_REG2;
__sfr __at 0xC7 MS_FM_DATA2;

char ms_detect(void) {
  char read_status = MS_FM_BASE_STATUS_REG1;
  if (read_status == 0xFF)
    return 0;
  return 1;
}

/* Para escribir en el primer banco de registros de la parte FM */
void ms_fm1_write(unsigned char reg, unsigned char data) {
  MS_FM_BASE_STATUS_REG1 = reg;
  MS_WAIT;
  MS_FM_DATA1 = data;
  MS_WAIT;
}

/* Para escribir en el segundo banco de registros de la parte FM */
void ms_fm2_write(unsigned char reg, unsigned char data) {
  MS_FM_REG2 = reg;
  MS_WAIT;
  MS_FM_DATA2 = data;
  MS_WAIT;
}

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15, 6, 6);
  switch (n) {
  case 1:
    printf("\n\rFAILED: fcb_open(): ");
    printf(name);
    break;
  case 2:
    printf("\n\rFAILED: fcb_close():");
    printf(name);
    break;
  case 3:
    printf("\n\rStop Kidding, run me on MSX2 !");
    break;
  case 4:
    printf("Not a fui instrument. It doesn't start by FINS");
    break;
  case 5:
    printf("No Moonsound or OPL4 compatible found");
    break;
  }

  Exit(0);
}

int FT_openFile(char *file_name)
{
  FT_SetName(&file, file_name);
  if (FcbOpen(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    printf("Ha petat\n\r");
    return (0);
  }
  printf("Fitxer obert\n\r");
}

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

__at 0x8000 char byteLleg[5];

void main() {
  printf("Comencem\n\r");

  if(!ms_detect()) {
    FT_errorHandler(5, "");
  }
  
  FT_openFile("op4bell.fui");
  printf("File opened\n\r");

  // Inicialitzem placa indicant que usarem els registres OPL3 i OPL4 (suposo que el OPL4 és pel Wave)
  ms_fm2_write(0x05, 3);

  char channel = 2; // Número canal, el segon, Referència documentació compta des d'1 , però pels registres és 0
  char slots[4];

  // Llegim que sigui FINS el fitxer
  FcbRead(&file, byteLleg, 4);
  if(byteLleg[0]!='F' || byteLleg[1]!='I' || byteLleg[2]!='N' || byteLleg[3]!='S'){
    FT_errorHandler(4,"");
  }

  // Llegim format version i instrument type
  FcbRead(&file, byteLleg, 4);
  printf("Read first 4 bytes\n\r");

  // Llegim si té nom l'instrument o ja trobem FM
  char trobatFM = 0;
  while (trobatFM == 0) {
    FcbRead(&file, byteLleg, 1);
    if (byteLleg[0] == 'M') {
      // Hi ha nom de l'instrument. Llegim fins al caràcter 0
      FcbRead(&file, byteLleg, 1);
      if (byteLleg[0] == 'A') {
        printf("Nom Instrument trobat\n\r");
        do {
          FcbRead(&file, byteLleg, 1);
        } while (byteLleg[0] != 0);
      }
    }

    if (byteLleg[0]=='F') {
      FcbRead(&file, byteLleg, 1);
      if (byteLleg[0]=='M') {
        trobatFM = 1;
      }
    }
  }
  printf("FM command found\n\r");

  // Llegim els bytes següent de FM que són el tamany del bloc. ¿S'han de girar per convertir-lo a enter (little endian)?
  int blockLength;
  FcbRead(&file, &blockLength,2);
  printf("Size FM bloc %d\n\r", blockLength);

  int numBytesLLegitsBlockFM = 0; // És un comptador dels bytes que s'han llegit per comparar al final si s'ha fet tot correctament

  // Llegim el número d'Ops que utilitza, 2 ó 4
  char numOperands;
  FcbRead(&file, byteLleg,1);
  numBytesLLegitsBlockFM ++;
  if (byteLleg[0] & 2 == 2) {
    numOperands = 2;
    slots[0] = 1; // Documentació comença des de 1, seria el 3 per la documentació, però registres van des del 0
    slots[1] = 4;
  } else {
    numOperands = 4;
    slots[0] = 2; // També comença indexat a 0, en comptes de la numeració de la documentació
    slots[1] = 0xa; // L'ordre és primer els sots 0 del canal i després del slot 1
    slots[2] = 5;   
    slots[3] = 0xd; // No van seguits els números dels slots, hi ha un salt cada dos registres (pàg 36)
  }
  printf("Numero op: %d\n\r", numOperands);
  
  // Alg i FB
  FcbRead(&file, byteLleg,1);
  // Si els operands són 4 hem de dir quin canal utilitzarem. En aquest exemple el 3, per tant el registre 0x04 ha de tenir un 4
  // Segons el tipus de connexió que volem hem d'escriure el registre C0-C8. Si és de tipus 2 usarem 1 bit i si és de tipus 4 dos bits
  char alg = (byteLleg[0] & 0xF0)>>4;
  char fb = (byteLleg[0] & 0x07)<<1;
  printf("alg: %d, fb: %d\n\r",alg,fb);

  // S'ha d'actualitzar el registre 5 abans per indicar quin tipus d'opll és.
  if (numOperands == 4) {
    ms_fm2_write(0x04,0x04); // Configurem canal 3 com a 4 operands. Està en el segon bloc
    // El registre 0xc? no només conté el FB i l'algoritme de connexió, sinó que també diu per a on ha de sonar el canal, dreat, esquerra 0 tots dos. Si no s'activa no sona. És el que m'havia passat. A part la conversió de vgm2txt no indica res d'aquesta combinació i per això em pensava que sortia igual. Sort que al final vaig posar els registres del DuesNotes directament al programa i vaig veure que funcionava. Els vaig copiar a la meva part i vaig anar traient línies fins que van quedar aquests registres que feien que sonés
    // I això que ja ho tenia explicat amb aquesta variable que no vaig arribar a utilitzar char stereo = 0x30; // Que sonin pels dos canals, segons explicació web opl3, la documentació tècnica sembla més complicada

    // Aquests són els bits 4 i 5. Diu la documentació que el 6 i 7 són per sortir per un altre canal i afegir-hi un SSFF, un modulador d'ona
    // Poso un 0x30 per activar-los
    switch(alg){
    case 0:
      ms_fm1_write(0xc0 + channel, 0x30 | fb ); // El FB només afecta el primer slot del canal. Però al furnace fa que agi a tots dos canals
      ms_fm1_write(0xc0 + channel +3, 0x30 | fb);
      break;
    case 1:
      // It is algorithm 3 in the documentation
      ms_fm1_write(0xc0 + channel, 0x31 | fb );
      ms_fm1_write(0xc0 + channel +3, 0x30 | fb);
      break;
    case 2:
      ms_fm1_write(0xc0 + channel, 0x30 | fb );
      ms_fm1_write(0xc0 + channel +3, 0x31 | fb);
      break;
    case 3:
      ms_fm1_write(0xc0 + channel, 0x31 | fb );
      ms_fm1_write(0xc0 + channel +3, 0x31 | fb);
      break;
    }
  } else {
    ms_fm1_write(0x04,0);
    ms_fm1_write(0xc0 + channel, 0x30 | fb);
    ms_fm1_write(0xc0 + channel + 3, 0x30 | fb);
  }


  // Els altres dos bytes crec que no apliquen a OPL
  FcbRead(&file, byteLleg,2);

  // KSR | DT | MULT // El DT no śe què és ¿?
  // Al byte 0x20 hi ha AM, VIB, EGT, KSR i Mult, Aquest es troben escampats els següents 5 bytes, els llegim tots per treballar-los

  for (char k = 0; k < numOperands; k++) {
    printf("** Configurant slot %d\n\r",slots[k]);
    FcbRead(&file, byteLleg, 5);

    char KSR = (byteLleg[0] >> 7) & 1; // En el fui és sempre 0, però el vgm del furnace els posa tots a 1????? Preguntar discord, tampoc hi ha botó en el disseny dels instruments. Potser ja ho havia comentat
    char DT = (byteLleg[0] >> 4) & 7; // No usat
    char mult = byteLleg[0] & 0x0F;
    char SUS = (byteLleg[1] >> 7) & 1; // No usat
    char TL = byteLleg[1] & 0x7F;
    char RS = (byteLleg[2] >> 6) & 3;
    char VIB = (byteLleg[2] >> 5) & 1;
    char AR = (byteLleg[2]) & 0x0F; //Docu furnace de 5 bits però OPL4 només 4
    char AM = (byteLleg[3] >> 7) & 1;
    char KSL = (byteLleg[3] >> 5) & 3;
    char DR = byteLleg[3] & 0x0F; //Docu furnace de 5 bits però OPL4 només 4, per això faig 0x0F en lloc de 0x1F
    char EGT = (byteLleg[4] >> 7 ) & 1;
    char KVS = (byteLleg[4] >> 5) & 3;
    char D2R = byteLleg[4] & 0x1F;

    ms_fm1_write(0x20 + slots[k], (AM<<7) | (VIB<<6) | (EGT<<5) | (KSR<<4) | mult); // Estic posant malament el KSR i l'EG
    ms_fm1_write(0x40 + slots[k], (KSL<<6) | TL);
    ms_fm1_write(0x60 + slots[k], (AR<<4) | DR);

    // SL RR
    FcbRead(&file, byteLleg,1);
    ms_fm1_write(0x80 + slots[k], byteLleg[0]);

    // DVB i SSG que no utilitzem
    FcbRead(&file, byteLleg,1);

    // DAM | DT2 | WS
    // Falten els del BDH 3 primers, que suposo que seran els DAM, però ho hauria de preguntar
    FcbRead(&file, byteLleg,1);
    // Els WS és el Wave Select
    ms_fm1_write(0xE0 + slots[k], byteLleg[0] & 0x07);
    printf("KSR: %d, DT: %d, mult: %d\n\r",KSR,DT,mult);
    printf("SUS: %d, TL: %d, RS: %d\n\r",SUS,TL,RS);
    printf("VIB: %d,AR: %d,AM: %d,KSL:%d\n\r",VIB,AR,AM,KSL);
    printf("DR: %d,EGT: %d,KVS: %d,D2R: %d\n\r",DR,EGT,KVS,D2R);
  }
  // Configuro que és sense drums
  ms_fm1_write(0xBD, byteLleg[0] & 0xE0);


  // Ara que ja tenim el canal definit, passem a tocar les notes
  unsigned int f_number = 582;
  unsigned char block = 4;
  ms_fm1_write(0xA0 + channel, f_number & 0x00ff);
  ms_fm1_write(0xB0 + channel, 0x20 | ((block & 0x07) << 2) | (f_number >> 8));

  printf("Ha sonat el A4\n\r");
  WAIT(100);

  // PAssem a A2
  // Primer fem l'anterior nota a off
  ms_fm1_write(0xB0 + channel, 0x00 | ((block & 0x07) << 2) | (f_number >> 8));
  // La tornem a on amb la nova configuració
  block = 2;
  ms_fm1_write(0xB0 + channel, 0x20 | ((block & 0x07) << 2) | (f_number >> 8));

  WAIT(100);
  printf("Ha sonat el A2. \n\r");
  // Apaguem la nota
  ms_fm1_write(0xB0 + channel, 0x00 | ((block & 0x07) << 2) | (f_number >> 8));
  //  printf("ByteLleg: %d\n", debug_byteLlegit);

  Exit(0);
}
