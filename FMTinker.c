#include "../fusion-c/include/msx_fusion.h"
#include <stdio.h>
#include <string.h>

void WriteOPLLreg_z80(char reg, char value) __naked {
  // Escrivim els registres a l'OPLL tal i com vam fer a les proves de FM_scratch
  __asm
    ld iy,#2
    add iy,sp ;Bypass the return addess of the function. Totes les funcions en asm del llibre de fusion-c fan primer aquest pas

    ld D,(IY) ; reg
    ld E,1(IY)  ; value

    ex af, af'
    ld A,D
    out (#0x7C),A
    ex af, af'
    LD A,E
    out (#0x7D),A
    ex (SP),HL
    ex (SP),HL
    ret
  __endasm;
}

void WriteOPLLreg_TR(char reg, char value)__naked {
  // La mateixa funció que WriteOPLLReg però per Turbo-R, els temps són més ràpids. Basat en comentaris de https://msx.org/forum/msx-talk/development/msx-music-player-using-r800#comment-436319
  __asm
    ld iy,#2
    add iy,sp 

    ld D,(IY) ; reg
    ld E,1(IY)  ; value

    ex af, af'
    ld A,D
    out (#0x7C),A
    ex af, af'
    ex af, af'
    ex af, af'
    ex af, af'
    LD A,E
    out (#0x7D),A
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ex (SP),HL
    ret
  __endasm;
}

// https://www.msx.org/forum/msx-talk/graphics-and-music/msx-music-playback-artifacts Aquí expliquen el canvi de nota
void main() {
  Screen(0);
  WriteOPLLreg_TR(0x30, 0xF0); // Electric guitar
  WriteOPLLreg_TR(0x10, 0xAD);
  WriteOPLLreg_TR(0x20, 0x38);
  Print("Sonant nota A4 - La\n\r"); 
  WaitForKey();
  // Canviem de nota
  WriteOPLLreg_TR(0x20, 0x04);
  WriteOPLLreg_TR(0x10, 0xAD);
  WriteOPLLreg_TR(0x20, 0x34);
  Print("Sonant nota A2 - La\n\r");
  WaitForKey();

  // Canvi instrument
  WriteOPLLreg_TR(0x20, 0x0A);
  WriteOPLLreg_TR(0x30, 0x10); // violí
  WriteOPLLreg_TR(0x10, 0xAD);
  WriteOPLLreg_TR(0x20, 0x38);
  Print("Sonant nota A4 - La; violi\n\r"); // Escriure nom nota en anglès i en català
  WaitForKey();
  // Canviem de nota
  WriteOPLLreg_TR(0x20, 0x04);
  WriteOPLLreg_TR(0x10, 0xAD);
  WriteOPLLreg_TR(0x20, 0x34);
  Print("Sonant nota A2 - La; violi\n\r");
  WaitForKey();
  WriteOPLLreg_TR(0x20, 0x0A);
  WriteOPLLreg_TR(0x10, 0xAD);
  WriteOPLLreg_TR(0x20, 0x18);
  Print("Ha sonat nota A4 - La sense sustain; violi\n"); // Sona igual que les altres
  WaitForKey();
  WriteOPLLreg_TR(0x20, 0x08);
  Print("Apaguem la nota A4 - La sense sustain; violi\n"); // Sona igual que les altres
  WaitForKey();

  WriteOPLLreg_TR(0x20, 0x38);
  Print("Ha sonat nota A4 - Amb sustain; violi\n\r"); // Sona igual que les altres
  WaitForKey();
  WriteOPLLreg_TR(0x20, 0x28);
  Print("Hem apagat la nota A4 - Amb sustain violi\n"); // Sona igual que les altres. Instrument com piano ja cau i no es nota
  WaitForKey();

  // Ara fem una polifonia, el do major, g3,e3,c3
  WriteOPLLreg_TR(0x30, 0x50); // Clarinet
  WriteOPLLreg_TR(0x31, 0x60); // Oboe
  WriteOPLLreg_TR(0x32, 0x90); // Horn

  WriteOPLLreg_TR(0x10, 0x34); // g3 - 0x134
  WriteOPLLreg_TR(0x11, 0xCD); // c3 - 0xCD
  WriteOPLLreg_TR(0x12, 0x03); // e3 - 0x103

  WriteOPLLreg_TR(0x20, 0x37); // g3 - 0x134
  WriteOPLLreg_TR(0x21, 0x36); // c3 - 0xCD
  WriteOPLLreg_TR(0x22, 0x37); // e3 - 0x103
  Print("Esta sonant el do major per 3 canals\n\r");
  WaitForKey();
  WriteOPLLreg_TR(0x31, 0x93); // Oboe
  Print("Hem baixat el volum de l'oboe");
  WaitForKey();

  WriteOPLLreg_TR(0x20, 0x27); // g3 - 0x134
  WriteOPLLreg_TR(0x21, 0x26); // c3 - 0xCD
  WriteOPLLreg_TR(0x22, 0x27); // e3 - 0x103

  // Posem tots els keys en off
  WriteOPLLreg_TR(0x20, 0x0); // g3 - 0x134
  WriteOPLLreg_TR(0x21, 0x0); // c3 - 0xCD
  WriteOPLLreg_TR(0x22, 0x0); // e3 - 0x103

  Exit(0);
}
