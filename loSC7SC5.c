/* Carreguem la imatge en sc7 i fem les animacions en sc5 */

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include <string.h>
#include <stdio.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC 2560*2

unsigned char LDbuffer[BUFFER_SIZE_SC];
char mypalette[16 * 4];

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=BUFFER_SIZE_SC;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}


void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("imatge.sc7", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC); // Carreguem la imatge
  FT_LoadPalette("image.pl7", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);
}

#define POS_Y_MAONS 160
#define TEMPS_CAIGUDA_MAONS 10

#define HALT __asm halt __endasm // wait for the next interrupt
void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

void main(){
  Screen(7);
  init_pantalla_joc();
  Screen(5);
  // La paleta
  SetColorPalette(9, 6, 1, 0);
  // Es divideixen els punts en les pàgines 1 i 3. Hauré d'anar copiant línia a línia per separar-ho
  // Construcció gat
  for (int k = 0; k < 8; k++) {
    HMMM(26 + 2 * k, 15 + 256, 10 + (4 * k), 10, 2, 20);
    HMMM(24 + 2 * k, 15 + 768, 8 + (4 * k), 10, 2, 20);
  }
  // Construcció man
  for (int k = 0; k < 8; k++) {
    HMMM(26 + 2 * k, 36 + 256, 10 + (4 * k), 40, 2, 20);
    HMMM(24 + 2 * k, 36 + 768, 8 + (4 * k), 40, 2, 20);
  }

  // Extret de PantInic.c_mod
  // Els maons tarden 8 animacions en estar al lloc, els de la filera d'abaix
  char pos_mao_anim =   0;
  char numero_animacio_caracters = 0;
  char num_animacions;
  char pos_x = 200;
  char pos_x_gat = 0; 

  for ( num_animacions = 0; num_animacions < 10;
                           num_animacions++) {
    // Brick central i caràcters
    
    HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);

    // Construcció gat
    for (int k = 0; k < 8; k++) {
      HMMM(24 + (numero_animacio_caracters*16) + (2 * k), 15 + 256, pos_x_gat + 2 + (4 * k), POS_Y_MAONS + 10, 2, 20);
      HMMM(22 + (numero_animacio_caracters*16) + (2 * k), 15 + 768, pos_x_gat + (4 * k), POS_Y_MAONS+10, 2, 20);
    }
    // Construcció man
    for (int k = 0; k < 8; k++) {
      HMMM(120 + (numero_animacio_caracters*16) + (2 * k), 36 + 256, pos_x + 2 + (4 * k), POS_Y_MAONS, 2, 20);
      HMMM(118 + (numero_animacio_caracters*16) + (2 * k), 36 + 768, pos_x + (4 * k), POS_Y_MAONS, 2, 20);
    }
    char *text = sprintf("%d",num_animacions);
    PutText(80, 100, text, 1);
    WAIT(TEMPS_CAIGUDA_MAONS);
    // Borrem
    HMMM(200, 0, 82 + 32, pos_mao_anim, 32, 16);
    HMMM(200, 0, pos_x, POS_Y_MAONS, 4, 32); // Borrem els quadrats
    HMMM(200, 0, pos_x_gat, POS_Y_MAONS+10, 4, 32);
    pos_x = pos_x - 4;
    pos_x_gat = pos_x_gat + 4;
    pos_mao_anim = pos_mao_anim + 16;
    numero_animacio_caracters ++;
    if (numero_animacio_caracters == 6) {
      numero_animacio_caracters = 0;
    }
  }
  // L'últim maó queda borrat, el copiem totalment de nou
  HMMM(0, 256 + 128, 82 + 32, pos_mao_anim, 32, 32);

  WaitForKey();
  Screen(0);
  RestorePalette();
  Exit(0);
}
