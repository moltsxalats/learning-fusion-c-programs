//
// Fusion-C
// Example : Moving Sam Garcia The Cat 16x16 Sprites
//
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <stdio.h>
#include <string.h>
#include <math.h>

/* --------------------------------------------------------- */
/*  SPRITEs of Sam Garcia The Cat                            */
/* ========================================================= */

static const unsigned char body_idle_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00011000,
  0b00100000
};

static const unsigned char body_idle_pattern_right_2[] = {
  0b00100000,
  0b00100011,
  0b00011111,
  0b00001111,
  0b00000111,
  0b00000110,
  0b00000010,
  0b00000000
};

static const unsigned char body_idle_pattern_right_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char body_idle_pattern_right_4[] = {
  0b00010100,
  0b10011010,
  0b11110000,
  0b11110000,
  0b11110000,
  0b00111000,
  0b00100000,
  0b00000000
};

static const unsigned char detail_idle_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_right_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000001,
  0b00000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_right_3[] = {  
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_right_4[] = {
  0b00000000,
  0b00000100,
  0b00001110,
  0b00001110,
  0b00001100,
  0b10000100,
  0b10011000,
  0b00000000
};

static const unsigned char body_walk_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00110000
};

static const unsigned char body_walk_pattern_right_2[] = {
  0b01000000,
  0b01000001,
  0b00100011,
  0b00011111,
  0b00001111,
  0b00011100,
  0b00110000,
  0b00100000
};

static const unsigned char body_walk_pattern_right_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00010100
};

static const unsigned char body_walk_pattern_right_4[] = {
  0b00011010,
  0b00010000,
  0b10110000,
  0b11110000,
  0b11111000,
  0b11101100,
  0b00000100,
  0b00000000
};

static const unsigned char detail_walk_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_walk_pattern_right_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000010,
  0b00001100,
  0b00001000
};

static const unsigned char detail_walk_pattern_right_3[] = {  
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_walk_pattern_right_4[] = {
  0b00000100,
  0b00001110,
  0b00001110,
  0b00001100,
  0b00000100,
  0b00000010,
  0b00000010,
  0b00000000
};

static const unsigned char body_idle_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char body_idle_pattern_left_2[] = {
  0b00101000,
  0b01011001,
  0b00001111,
  0b00001111,
  0b00001111,
  0b00011100,
  0b00000100,
  0b00000000
};

static const unsigned char body_idle_pattern_left_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00011000,
  0b00000100
};

static const unsigned char body_idle_pattern_left_4[] = {
  0b00000100,
  0b11000100,
  0b11111000,
  0b11110000,
  0b11100000,
  0b01100000,
  0b01000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_left_2[] = {
  0b00000000,
  0b00100000,
  0b01110000,
  0b01110000,
  0b00110000,
  0b00100001,
  0b00011001,
  0b00000000
};

static const unsigned char detail_idle_pattern_left_3[] = {  
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_idle_pattern_left_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b10000000,
  0b00000000,
  0b00000000
};

static const unsigned char body_walk_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00101000
};

static const unsigned char body_walk_pattern_left_2[] = {
  0b01011000,
  0b00001000,
  0b00001101,
  0b00001111,
  0b00011111,
  0b00110111,
  0b00100000,
  0b00000000
};

static const unsigned char body_walk_pattern_left_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00001100
};

static const unsigned char body_walk_pattern_left_4[] = {
  0b00000010,
  0b00000010,
  0b11000100,
  0b11111000,
  0b11110000,
  0b00111000,
  0b00001100,
  0b00000100
};

static const unsigned char detail_walk_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_walk_pattern_left_2[] = {
  0b00100000,
  0b01110000,
  0b01110000,
  0b00110000,
  0b00100000,
  0b01000000,
  0b01000000,
  0b00000000
};

static const unsigned char detail_walk_pattern_left_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_walk_pattern_left_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b01000000,
  0b00110000,
  0b00010000
};

static const unsigned char body_impuls_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00110000
};

static const unsigned char body_impuls_pattern_right_2[] = {
  0b00100000,
  0b00100000,
  0b00100011,
  0b00111111,
  0b00011111,
  0b00001111,
  0b00001000,
  0b00011000
};

static const unsigned char body_impuls_pattern_right_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00010100
};

static const unsigned char body_impuls_pattern_right_4[] = {
  0b00011100,
  0b00010100,
  0b00100000,
  0b11100000,
  0b11100000,
  0b11100000,
  0b00100000,
  0b00100000
};

static const unsigned char detail_impuls_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_impuls_pattern_right_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000110,
  0b00000100
};

static const unsigned char detail_impuls_pattern_right_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_impuls_pattern_right_4[] = {
  0b00000000,
  0b00001000,
  0b00011100,
  0b00011100,
  0b00011000,
  0b00010000,
  0b00011000,
  0b00001000
};

static const unsigned char body_salt_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00100000
};

static const unsigned char body_salt_pattern_right_2[] = {
  0b00100000,
  0b00110000,
  0b00110011,
  0b00011111,
  0b00001111,
  0b00000110,
  0b00001100,
  0b00001000
};

static const unsigned char body_salt_pattern_right_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00101000,
  0b00111000,
  0b00110100
};

static const unsigned char body_salt_pattern_right_4[] = {
  0b00100000,
  0b01100000,
  0b11100000,
  0b11100000,
  0b11100000,
  0b00100000,
  0b01100000,
  0b00000000
};

static const unsigned char detail_salt_pattern_right_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_salt_pattern_right_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000001,
  0b00000010,
  0b00000110
};

static const unsigned char detail_salt_pattern_right_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00001000
};

static const unsigned char detail_salt_pattern_right_4[] = {
  0b00011100,
  0b00011100,
  0b00011100,
  0b00011100,
  0b00011000,
  0b00001000,
  0b00001000,
  0b00000000
};

static const unsigned char body_impuls_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00101000
};

static const unsigned char body_impuls_pattern_left_2[] = {
  0b00111000,
  0b00101000,
  0b00000100,
  0b00000111,
  0b00000111,
  0b00000111,
  0b00000100,
  0b00000100
};

static const unsigned char body_impuls_pattern_left_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00001100
};

static const unsigned char body_impuls_pattern_left_4[] = {
  0b00000100,
  0b00000100,
  0b10000100,
  0b11111100,
  0b11111000,
  0b11110000,
  0b00010000,
  0b00011000
};

static const unsigned char detail_impuls_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_impuls_pattern_left_2[] = {
  0b00000000,
  0b00010000,
  0b00111000,
  0b00111000,
  0b00011000,
  0b00001000,
  0b00011000,
  0b00010000
};

static const unsigned char detail_impuls_pattern_left_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_impuls_pattern_left_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b01100000,
  0b00100000
};

static const unsigned char body_salt_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00101000,
  0b00111000,
  0b00101100
};

static const unsigned char body_salt_pattern_left_2[] = {
  0b00000100,
  0b00000110,
  0b00000111,
  0b00000111,
  0b00000111,
  0b00000100,
  0b00000110,
  0b00000000
};

static const unsigned char body_salt_pattern_left_3[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000100
};

static const unsigned char body_salt_pattern_left_4[] = {
  0b00000100,
  0b00001100,
  0b11001100,
  0b11111000,
  0b11110000,
  0b01100000,
  0b00110000,
  0b00010000
};

static const unsigned char detail_salt_pattern_left_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00010000
};

static const unsigned char detail_salt_pattern_left_2[] = {
  0b00111000,
  0b00111000,
  0b00111000,
  0b00111000,
  0b00011000,
  0b00010000,
  0b00010000,
  0b00000000
};

static const unsigned char detail_salt_pattern_left_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char detail_salt_pattern_left_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b10000000,
  0b01000000,
  0b01100000
};

/* --------------------------------------------------------- */
/*  SPRITE energia Sam Garcia The Cat                        */
/* ========================================================= */

static const unsigned char energia_pattern_1[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char energia_pattern_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00100000,
  0b11000000,
  0b11000000
};

static const unsigned char energia_pattern_3[] = { 
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char energia_pattern_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000100,
  0b00000011,
  0b00000011
};

// Functions prototypes
char FT_wait(int);
int screen_limits_x(int);
int screen_limits_y(int);

/* --------------------------------------------------------- */
char FT_wait(int cicles)
{
  int i;
  for(i=0;i<cicles;i++) 
  {
    EnableInterupt();
    Halt();
  }
  return(0);
}

int screen_limits_x(int x)
{
  if( x < 0) 
  {
   x=0;
  }

  if(x > 200)
  {
   x=200; 
  }
  return x;
}

int screen_limits_y(int y)
{
  if( y < 0) 
  {
   y=0;
  }

  if(y > 160)
  {
   y=160; 
  }
  return y;
}

/* --------------------------------------------------------- */
void main( void ) {
    int x;
    int y;

    char strx[6];
    char stry[6];

    char stick;
    char space;    

    char mypalette[] = {
      0, 0,0,0, // transparent 
      1, 1,1,1, // black
      2, 1.7,5,2, // medium green
      3, 3.1,5.7,3.4, // light green 
      4, 2.4,2.3,6.1, // dark blue
      5, 3.5,3.2,6.6, // light blue 
      6, 5,2.5,2.2, // dark red
      7, 2.7,6,6.5, // cyan
      8, 6,2.7,2.4, // medium red
      9, 7,3.7,3.4, // light red
      10, 5.6,5.3,2.5, // dark yellow
      11, 6,5.7,3.7, // light yellow
      12, 1.5,4.4,1.7, // dark green
      13, 5,2.8,4.9, // magenta
      14, 5.6,5.6,5.6, // gray
      15, 7,7,7 // white
    };
    
    char LineColorsLayer4[16]= { 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4  };
    char LineColorsLayer14[16]= { 14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14 };
    
    char LineColorsLayer12[16]= { 12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12 };
    char LineColorsLayer3[16]= { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
    char LineColorsLayer2[16]= { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
    char LineColorsLayer1[16]= { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };


    x=79;
    y=50;

   
  if(ReadMSXtype()==3)  // IF MSX is Turbo-R Switch CPU to Z80 Mode
    {
       ChangeCPU(0);
    }
  
  SetColors(15,1,1);
  Screen(5);

  SetSC5Palette((Palette *)mypalette);
  
  SpriteReset();

  SpriteDouble();
  
  // The 16 x 16 pixels Sprite is made of 4 patterns

  SetSpritePattern( 0, body_idle_pattern_right_1,8 );
  SetSpritePattern( 1, body_idle_pattern_right_2,8 );
  SetSpritePattern( 2, body_idle_pattern_right_3,8 );
  SetSpritePattern( 3, body_idle_pattern_right_4,8 );

  SetSpritePattern( 4, detail_idle_pattern_right_1,8 );
  SetSpritePattern( 5, detail_idle_pattern_right_2,8 );
  SetSpritePattern( 6, detail_idle_pattern_right_3,8 );
  SetSpritePattern( 7, detail_idle_pattern_right_4,8 );

  SetSpritePattern( 8, body_walk_pattern_right_1,8 );
  SetSpritePattern( 9, body_walk_pattern_right_2,8 );
  SetSpritePattern( 10, body_walk_pattern_right_3,8 );
  SetSpritePattern( 11, body_walk_pattern_right_4,8 );

  SetSpritePattern( 12, detail_walk_pattern_right_1,8 );
  SetSpritePattern( 13, detail_walk_pattern_right_2,8 );
  SetSpritePattern( 14, detail_walk_pattern_right_3,8 );
  SetSpritePattern( 15, detail_walk_pattern_right_4,8 );

  SetSpritePattern( 16, body_idle_pattern_left_1,8 );
  SetSpritePattern( 17, body_idle_pattern_left_2,8 );
  SetSpritePattern( 18, body_idle_pattern_left_3,8 );
  SetSpritePattern( 19, body_idle_pattern_left_4,8 );

  SetSpritePattern( 20, detail_idle_pattern_left_1,8 );
  SetSpritePattern( 21, detail_idle_pattern_left_2,8 );
  SetSpritePattern( 22, detail_idle_pattern_left_3,8 );
  SetSpritePattern( 23, detail_idle_pattern_left_4,8 );

  SetSpritePattern( 24, body_walk_pattern_left_1,8 );
  SetSpritePattern( 25, body_walk_pattern_left_2,8 );
  SetSpritePattern( 26, body_walk_pattern_left_3,8 );
  SetSpritePattern( 27, body_walk_pattern_left_4,8 );

  SetSpritePattern( 28, detail_walk_pattern_left_1,8 );
  SetSpritePattern( 29, detail_walk_pattern_left_2,8 );
  SetSpritePattern( 30, detail_walk_pattern_left_3,8 );
  SetSpritePattern( 31, detail_walk_pattern_left_4,8 );

  SetSpritePattern( 32, body_impuls_pattern_right_1,8 );
  SetSpritePattern( 33, body_impuls_pattern_right_2,8 );
  SetSpritePattern( 34, body_impuls_pattern_right_3,8 );
  SetSpritePattern( 35, body_impuls_pattern_right_4,8 );

  SetSpritePattern( 36, detail_impuls_pattern_right_1,8 );
  SetSpritePattern( 37, detail_impuls_pattern_right_2,8 );
  SetSpritePattern( 38, detail_impuls_pattern_right_3,8 );
  SetSpritePattern( 39, detail_impuls_pattern_right_4,8 );

  SetSpritePattern( 40, energia_pattern_1,8 );
  SetSpritePattern( 41, energia_pattern_2,8 );
  SetSpritePattern( 42, energia_pattern_3,8 );
  SetSpritePattern( 43, energia_pattern_4,8 );

  SetSpritePattern( 44, body_salt_pattern_right_1,8 );
  SetSpritePattern( 45, body_salt_pattern_right_2,8 );
  SetSpritePattern( 46, body_salt_pattern_right_3,8 );
  SetSpritePattern( 47, body_salt_pattern_right_4,8 );

  SetSpritePattern( 48, detail_salt_pattern_right_1,8 );
  SetSpritePattern( 49, detail_salt_pattern_right_2,8 );
  SetSpritePattern( 50, detail_salt_pattern_right_3,8 );
  SetSpritePattern( 51, detail_salt_pattern_right_4,8 );

  SetSpritePattern( 52, body_impuls_pattern_left_1,8 );
  SetSpritePattern( 53, body_impuls_pattern_left_2,8 );
  SetSpritePattern( 54, body_impuls_pattern_left_3,8 );
  SetSpritePattern( 55, body_impuls_pattern_left_4,8 );

  SetSpritePattern( 56, detail_impuls_pattern_left_1,8 );
  SetSpritePattern( 57, detail_impuls_pattern_left_2,8 );
  SetSpritePattern( 58, detail_impuls_pattern_left_3,8 );
  SetSpritePattern( 59, detail_impuls_pattern_left_4,8 );

  SetSpritePattern( 60, body_salt_pattern_left_1,8 );
  SetSpritePattern( 61, body_salt_pattern_left_2,8 );
  SetSpritePattern( 62, body_salt_pattern_left_3,8 );
  SetSpritePattern( 63, body_salt_pattern_left_4,8 );

  SetSpritePattern( 64, detail_salt_pattern_left_1,8 );
  SetSpritePattern( 65, detail_salt_pattern_left_2,8 );
  SetSpritePattern( 66, detail_salt_pattern_left_3,8 );
  SetSpritePattern( 67, detail_salt_pattern_left_4,8 );

  SC5SpriteColors(1,LineColorsLayer4);
  SC5SpriteColors(2,LineColorsLayer14);
  SC5SpriteColors(3,LineColorsLayer12);

  Sprite16();
  
  // Printing initial coordinates on the Screen
  PutText(0,10,"SCREEN 5",0);  
    
  sprintf(strx, "%i", x);
  sprintf(stry, "%i", y);
  
  PutText(83,10,"(",0);
  PutText(91,10,strx,0);
  PutText(113,10,",",0);
  PutText(122,10,stry,0);
  PutText(144,10,")",0);

  // Positioning Sam on the Screen
  PutSprite (1,0,x,y,4);
  PutSprite (2,4,x,y,14);

  // Game loop
  while (Inkey()!=27)
  {  
    stick = JoystickRead(0);
    space = TriggerRead(0);

      // Idle
      if(stick!=0) {
        
        sprintf(strx, "%i", x);
        sprintf(stry, "%i", y);
        
        PutText(83,10,"(",0);
        PutText(91,10,strx,0);
        PutText(113,10,",",0);
        PutText(122,10,stry,0);
        PutText(144,10,")",0);        

      }
      
      // Up
      if(stick==1)
      {        
        y=(y-1);

        PutSprite (1,0,x,y,4);
        PutSprite (2,4,x,y,14);

        FT_wait(10);
      }

      // Right
      if(stick==3)
      {
        
        x=(x+1);
        x=screen_limits_x(x);

        if( x % 2 == 0) 
        {
          PutSprite (1,0,x,y,4);
          PutSprite (2,4,x,y,14);
        }

        if( x % 2 == 1) 
        {
          PutSprite (1,8,x,y,4);
          PutSprite (2,12,x,y,14);          
        }
        FT_wait(10);
      }

      // Down
      if(stick==5)
      {        
        y=(y+1);

        PutSprite (1,0,x,y,4);
        PutSprite (2,4,x,y,14);

        FT_wait(10);
      }

      // Right
      if(stick==7)
      {

        x=(x-1);
        x=screen_limits_x(x);

        if( x % 2 == 0) 
        {
          PutSprite (1,16,x,y,4);
          PutSprite (2,20,x,y,14);
        }

        if( x % 2 == 1) 
        {
          PutSprite (1,24,x,y,4);
          PutSprite (2,28,x,y,14);
        }
        FT_wait(10);
      }

      // Button A
      if(space==255 && stick==3)
      {
          PutSprite (1,32,x,y,4);
          PutSprite (2,36,x,y,14);

          FT_wait(10);

          // Impulse
          SC5SpriteColors(3,LineColorsLayer12);
          PutSprite (3,40,x,y,12);
          FT_wait(10);

          SC5SpriteColors(3,LineColorsLayer2);
          PutSprite (3,40,x,y,2);
          FT_wait(10);

          SC5SpriteColors(3,LineColorsLayer3);
          PutSprite (3,40,x,y,3);
          FT_wait(10);          

          SC5SpriteColors(3,LineColorsLayer1);
          PutSprite (3,40,x,y,1);
          FT_wait(10);

          // Jump
          x=x+20;
          y=y-20;

          PutSprite (1,44,x,y,4);
          PutSprite (2,48,x,y,14);

          FT_wait(10);

          // Back to the floor
          y=y+20;

          PutSprite (1,0,x,y,4);
          PutSprite (2,4,x,y,14);

      }
      // Button A
      if(space==255 && stick==7)
      {
          PutSprite (1,52,x,y,4);
          PutSprite (2,56,x,y,14);

          FT_wait(10);

          // Impulse
          SC5SpriteColors(3,LineColorsLayer12);
          PutSprite (3,40,x,y,12);
          FT_wait(10);

          SC5SpriteColors(3,LineColorsLayer2);
          PutSprite (3,40,x,y,2);
          FT_wait(10);

          SC5SpriteColors(3,LineColorsLayer3);
          PutSprite (3,40,x,y,3);
          FT_wait(10);          

          SC5SpriteColors(3,LineColorsLayer1);
          PutSprite (3,40,x,y,1);
          FT_wait(10);

          // Jump
          x=x-20;
          y=y-20;

          PutSprite (1,60,x,y,4);
          PutSprite (2,64,x,y,14);

          FT_wait(10);

          // Back to the floor
          y=y+20;

          PutSprite (1,16,x,y,4);
          PutSprite (2,20,x,y,14);

      }

  }
  Screen(0);
  Exit(0);
}
