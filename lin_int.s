
PORT_1 .equ #0x99 

.area _CODE

_lin_int::
  ld  	a, (#Flag_IN)
  or  	a
  jp  	nz, _else
                                ; .then:
  ld 		a, #1
  ld  	(#Flag_IN), a
  call  	_sub_470
  xor  	a
  ld  	(#Flag_IN), a
  ld  	(#Counter_T), a
  ret
_else:
                                ; T=T+1
  ld  	hl, (#Counter_T)
  inc		(hl)

			                          ; IF T=100 THEN T=0:IN=0
  ld  	a, (hl)
  cp  	#100
  ret  	nz

	xor  	a
  ld  	(#Counter_T), a
  ld  	(#Flag_IN), a
  ret


_sub_470:
                                ; Example interrupt handler
                                ; IF (VDP(-1)AND1)=1 THEN 530 ' Is this line interrupt?
	ld  	b, #1
  call 	RDSTATUSREG
  
  ld  	a, #1
  and  	b
  
  cp  	#1
  
  jp   	z, _sub_470_line_530
  
                                ; ' This was not line interrupt, so it's propably VBLANK
                                ; ' VBLANK happens when screen has been drawn.
                                ; VDP(24)=0 ' Upper part of screen shows still picture
	ld  	b, #0		; data to write
  ld  	c, #23		; register number (9 to 24	Control registers 8 to 23	Read / Write	MSX2 and higher)
  call  	WRTVDP_without_DI_EI		; Write B value to C register
  di
  
	ret

_sub_470_line_530:
                                ; ' Here we handle line interrupt
                                ; ' Lower part of screen jumps
                                ; VDP(24)=P:P=ABS(SIN(R/20)*100):R=R+1
                                ; RETURN
  ld  	a, (#Var_P)
	ld  	b, a		; data to write
  ld  	c, #23		; register number (9 to 24	Control registers 8 to 23	Read / Write	MSX2 and higher)
  call  	WRTVDP_without_DI_EI		; Write B value to C register
                                ;di
  
  ld  	a, (#Direction)
  ld  	b, a
  ld  	a, (#Var_P)
  add  	a, b
  ld  	(#Var_P), a

                                ; invert direction flag
	cp  	#0
  jp  	z, _setDirection_Plus_1
	cp  	#64
  jp  	z, _setDirection_Minus_1
  
	ret
  
_setDirection_Plus_1:
	ld  	a, #1
  ld  	(Direction), a
  ret
_setDirection_Minus_1:
	ld  	a, #-1
  ld  	(Direction), a
  ret


RDSTATUSREG:
                                ; -> Write the registre number in the r#15 (these 7 lines are specific MSX2 or newer)
	ld	a,(#0x0007)	; Main-ROM must be selected on page 0000h-3FFFh
	inc	a
	ld	c,a		; C = CPU port #99h (VDP writing port#1)
	                              ;di		; Interrupts must be disabled here
	out	(c),b
	ld	a,#15 + #0x080
	out	(c),a
                                ; <-
  
	ld	a,(#0x006)	; Main-ROM must be selected on page 0000h-3FFFh
	inc	a
	ld	c,a		; C = CPU port #99h (VDP reading port#1)
	in	b,(c)	; read the value to the port#1
  
                                ; -> Rewrite the registre number 0 in the r#15 (these 8 lines are specific MSX2 or newer)
	ld	a,(#0x007)	; Main-ROM must be selected on page 0000h-3FFFh
	inc	a
	ld	c,a		; C = CPU port #99h (VDP writing port#1)
	xor	a
	out	(c),a
	ld	a,#15 + #0x080
	out	(c),a
	                              ;ei		; Interrupts can be enabled here
                                ; <-
	ret

                                ; Write B value to C register
WRTVDP_without_DI_EI:
  ld 		a, b
                                ;di
  out 	(#PORT_1),a
  ld  	a, c
  or  	#128
                                ;ld 	a, regnr + 128
                                ;ei
  out 	(#PORT_1), a
  ret

  ;; **************************************

Flag_IN: .db #0
Counter_T: .db #0
Var_P:		.db #0
Var_AD:		.dw #0
Direction:  .db #0
