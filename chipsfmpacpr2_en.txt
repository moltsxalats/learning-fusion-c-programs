            M S X - M U S I C   R E G I S T E R S
           =======================================

                         WRITE ONLY

The registers of the  MSX-MUSIC system, as found in: FM-Pac,
FM-Stereo-Pak, MSX2+  and  TurboR computers, are  controlled
via  ports  &H7C and &H7D. The  control  works  according to
the  same  principle  as  with  MSX-AUDIO  and  the PSG. The
register  is  specified   in  7C  and   the   data   in  7D.
Unfortunately, these are Write Only ports, so they cannot be
read.

                       SOUND SETTINGS

The  OPLL  data   (original  instrument)  is   specified  in
registers 0-7. This  data corresponds  to  the data  you can
retrieve in the  SynthSaurus Sound Editor, so you can easily
retrieve  a  homemade instrument  and  write those values to
registers 0-7.

The following is the structure of registers 0-7 (OPLL instr)

Reg.nr.    Bit:    Function:
------------------------------------------------------------
0,1        0-3     Multi sample waves/harmonic relations
           4       Rate key scale
           5       Latching/sprouting (1=Latch 0=Latch)
           6       Vibration on/off (1=on 0=off)
           7       Amplitude modulation (1=on 0=off)

2          0-5     Modulation index
           6-7     Level key scale

3          0-2     FM recoil
           3-4     Load-bearing and modulated waveform
                   link (FM/AM)
           6-7     Level key scale

4,5        0-3     Decay change
           4-7     Attack change (swelling)

6,7        0-3     Opening the change control
           4-7     Major of Attack/Decay
------------------------------------------------------------


                    INSTRUMENT CONTROL

The   registers   printed   below   are  for  the  instrument
selection/frequency/octave/volume and the so-called Sustain.

Reg.nr.    Bit:    Function:
------------------------------------------------------------
&H10-&H18  0-7     Frequentie noot LSB (8 bits)

&H20-&H28  0       Frequentie noot MSB (1 bit)
           1-3     Octaaf nr. (0-7, 0=octaaf 1, 7=octaaf 8)
           4       Key aan/uit (1=aan, 0=uit)
           5       Sustain (vasthouden, 1=aan, 0=uit)

&H30-&H38  0-3     Volume (0=vol.15, F=vol.0 !)
           4-7     Instrumenten keuze (0=original, <>0=FM
                                       instrument)
------------------------------------------------------------


                       FM-INSTRUMENTEN

Het MSX-MUSIC systeem heeft 15  voorgeprogrammeerde  klanken
die afzonderlijk van elkaar kunnen worden gebruikt.

0 = Original (zelf ontworpen instr. zie KLANK-INSTELLINGEN)
1 = Viool                    9 = Hoorn
2 = Guitaar                  A = Synthesizer
3 = Piano                    B = Harpsichore
4 = Fluit                    C = Vibraphone
5 = Clarinet                 D = Synthesizer Bas
6 = Hobo                     E = ElektrPiano2/Acoust. Bas
7 = Trompet                  F = ElektrPiano1/ElektrGitaar
8 = Orgel


                           RHYTHM

Register &H0E contains the drum selection, however bit 5 must
be written before a drum can be heard. The drums  can also be
slightly changed, but rather limited and a bit chaotic. Below
is a table of the drum values that can be written in register
&H0E:

bit 0 = HiHat                bit 3 = Snare drum/Field drum
bit 1 = Cymbal               bit 4 = Bass drum
bit 2 = TomTom               bit 5 = selector (1 = Rhythm,
                                      0 = Instrument)

Of course, multiple drums  can be controlled  simultaneously.
The rhythm sound is structured as follows:

Drum type:    Reg.   Bit:    Function:
------------------------------------------------------------
Bass drum     &H16   0-7     Frequency LSB (8 bits)
              &H26   0       Frequency MSB (1 bit)
                     1-3     Octave (0-7)
              &H36   0-3     Volume Bass drum

Snare &       &h17   0-7     Frequency LSB (8 bits)
HiHat         &H27   0       Frequency MSB (1 bit)
                     1-3     Octave (0-7)
              &H37   0-3     Volume Snare
                     4-7     Volume HiHat

Cymbal &      &H18   0-7     Frequency LSB (8 bits)
TomTom        &H28   0       Frequency MSB (1 bit)
                     1-3     Octave (0-7)
              &H38   0-3     Volume Cymbal
                     4-7     Volume TomTom
------------------------------------------------------------


                       BASIC REGISTERS

De registers van het MSX-MUSIC systeem worden, onder BASIC!,
bijgehouden vanaf adres &HF9C0 tot &HF9C0+&H38.  De  instel-
lingen van deze registers zijn na het  commando  CALL  MUSIC
gevuld met de volgende waardes:

                                  Reg.nr.        Waarde:
------------------------------------------------------------
Register paar 1:   Instruments    &H10-&H15      &H56
                   Drums          &H16           &H20
                                  &H17           &H50
                                  &H18           &HC0

Register paar 2:   Instruments    &H20-&H25      &H00
                   Drums          &H26           &H05
                                  &H27           &H05
                                  &H28           &H01

Register paar 3:   Instruments    &H30-&H35      &H30
                   Drums          &H36           &H01
                                  &H37           &H11
                                  &H38           &H11
------------------------------------------------------------


                           GEBRUIK

Wanneer men deze informatie gaat gebruiken  in  bijvoorbeeld
een interrupt gestuurd muziekstuk, dan moet u met de volgen-
de dingen rekening houden.

1) Als er een nieuwe frequentie van een instrument wordt op-
   gegeven, moet eerst bit 4 van het desbetreffende register
   uit worden geschakeld. Ter verduidelijking: als in kanaal
   1 een nieuwe frequentie moet worden ingevoerd,  dan  moet
   bit 4 van register &H10 worden uitgezet. Daarna wordt  de
   frequentie geschreven, na dit alles wordt bit 4 weer aan-
   gezet (reg. &H10). Voor andere kanalen  wordt  natuurlijk
   een ander register beschreven.

2) Als er een  instrument  wordt  gespeeld  dat  'uitsterft'
   (Release), dan moete r herhaling voor in machinetaal:

         LD   B,3    ;ongeveer 3!
   LOOP: DJNZ LOOP

3) De frequenties van de noten zijn:

   Noot:     LSB       MSB (bit 0 van reg &H20-&H28)
   ---------------------------------------------------------
   C         &HAD      0
   C#        &HB7      0
   D         &HC2      0
   D#        &HCD      0
   E         &HD9      0
   F         &HE6      0
   F#        &HF4      0
   G         &H03      1
   G#        &H12      1
   A         &H22      1
   A#        &H34      1
   B         &H46      1
   ---------------------------------------------------------

Met deze informatie kunt u nu in  staat  zijn  om  via  deze
registers het MSX-MUSIC systeem aan te sturen. Veel succes!

                            R.M.

(Even voor alle duidelijkheid: ik weet niet wie R.M. is, 
maar ik ben het niet! - RM-FCS)
