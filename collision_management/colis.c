//Documentation section

//
// colis.c
// Fusion-C hardware and software collision example
// Here we are going to test both hadware and software collisions via an example
// MoltSXalats 2024
//

#include "fusion-c/header/vdp_graph2.h"
#include "fusion-c/header/msx_fusion.h"
#include "fusion-c/header/vdp_sprites.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>


/* --------------------------------------------------------- */
/*  SPRITE square with right opening                         */
/* ========================================================= */

static const unsigned char left_object_pattern_1[] = {
  0b00000000,
  0b00000000,
  0b00111111,
  0b00111111,
  0b00110000,
  0b00110000,
  0b00110000,
  0b00110000
};

static const unsigned char left_object_pattern_2[] = {
  0b00110000,
  0b00110000,
  0b00110000,
  0b00110000,
  0b00111111,
  0b00111111,
  0b00000000,
  0b00000000
};

static const unsigned char left_object_pattern_3[] = {
  0b00000000,
  0b00000000,
  0b11111100,
  0b11111100,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char left_object_pattern_4[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b11111100,
  0b11111100,
  0b00000000,
  0b00000000
};

/* --------------------------------------------------------- */
/*  SPRITE square with left opening.                         */
/* ========================================================= */

static const unsigned char right_object_pattern_1[] = {
  0b00000000,
  0b00000000,
  0b00111111,
  0b00111111,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000
};

static const unsigned char right_object_pattern_2[] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00000000,
  0b00111111,
  0b00111111,
  0b00000000,
  0b00000000
};

static const unsigned char right_object_pattern_3[] = {
  0b00000000,
  0b00000000,
  0b11111100,
  0b11111100,
  0b00001100,
  0b00001100,
  0b00001100,
  0b00001100
};

static const unsigned char right_object_pattern_4[] = {
  0b00001100,
  0b00001100,
  0b00001100,
  0b00001100,
  0b11111100,
  0b11111100,
  0b00000000,
  0b00000000
};

/* --------------------------------------------------------- */
/*  SPRITE traffic light                                           */
/* ========================================================= */

static const unsigned char semaphore_pattern_1[] = {
  0b00000000,
  0b00000111,
  0b00001111,
  0b00011111,
  0b00111111,
  0b01111111,
  0b01111111,
  0b01111111
};

static const unsigned char semaphore_pattern_2[] = {
  0b01111111,
  0b01111111,
  0b00111111,
  0b00011111,
  0b00001111,
  0b00000111,
  0b00000000,
  0b00000000
};

static const unsigned char semaphore_pattern_3[] = {
  0b00000000,
  0b11000000,
  0b11100000,
  0b11110000,
  0b11111000,
  0b11111100,
  0b11111100,
  0b11111100
};

static const unsigned char semaphore_pattern_4[] = {
  0b11111100,
  0b11111100,
  0b11111000,
  0b11110000,
  0b11100000,
  0b11000000,
  0b00000000,
  0b00000000
};

// Functions prototypes
void FT_Wait(int);
int screen_limits_x(int);
int screen_limits_y(int);

/* --------------------------------------------------------- */
void FT_Wait(int cicles) {
  unsigned int i;

  for(i=0;i<cicles;i++)
    {}
}

int screen_limits_x(int x)
{
  if( x < 0) 
  {
   x=0;
  }

  if(x > 200)
  {
   x=200; 
  }
  return x;
}

int screen_limits_y(int y)
{
  if( y < 0) 
  {
   y=0;
  }

  if(y > 160)
  {
   y=160; 
  }
  return y;
}

/* --------------------------------------------------------- */
void main( void ) {
    int xlefobj;
    int ylefobj;

    int xrigobj;
    int yrigobj;

    char strx[6];
    char stry[6];

    char stick;
    char key;    

    char mypalette[] = {
      0, 0,0,0, // transparent 
      1, 1,1,1, // black
      2, 1.7,5,2, // medium green
      3, 3.1,5.7,3.4, // light green 
      4, 2.4,2.3,6.1, // dark blue
      5, 3.5,3.2,6.6, // light blue 
      6, 5,2.5,2.2, // dark red
      7, 2.7,6,6.5, // cyan
      8, 6,2.7,2.4, // medium red
      9, 7,3.7,3.4, // light red
      10, 5.6,5.3,2.5, // dark yellow
      11, 6,5.7,3.7, // light yellow
      12, 1.5,4.4,1.7, // dark green
      13, 5,2.8,4.9, // magenta
      14, 5.6,5.6,5.6, // gray
      15, 7,7,7 // white
    };
    
    char LineColorsLayer04[16]= { 4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4  };
    char LineColorsLayer14[16]= { 14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14 };
    
    char LineColorsLayer12[16]= { 12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12 };
    char LineColorsLayer03[16]= { 3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3 };
    char LineColorsLayer02[16]= { 2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2 };
    char LineColorsLayer01[16]= { 1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1 };

    char LineColorsLayer13[16]= { 13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13 };

    xlefobj=32;
    ylefobj=32;

    xrigobj=64;
    yrigobj=64;

   
  if(ReadMSXtype()==3)  // IF MSX is Turbo-R Switch CPU to Z80 Mode
    {
       ChangeCPU(0);
    }
  
  SetColors(15,1,1);
  // 256 x 212 pixel
  Screen(5);

  SetSC5Palette((Palette *)mypalette);
  
  SpriteReset();

  SpriteDouble();
  
  // The 16 x 16 pixels Sprite is made of 4 patterns

  SetSpritePattern( 0, left_object_pattern_1,8 );
  SetSpritePattern( 1, left_object_pattern_2,8 );
  SetSpritePattern( 2, left_object_pattern_3,8 );
  SetSpritePattern( 3, left_object_pattern_4,8 );

  SetSpritePattern( 4, right_object_pattern_1,8 );
  SetSpritePattern( 5, right_object_pattern_2,8 );
  SetSpritePattern( 6, right_object_pattern_3,8 );
  SetSpritePattern( 7, right_object_pattern_4,8 );

  SetSpritePattern( 8, semaphore_pattern_1,8 );
  SetSpritePattern( 9, semaphore_pattern_2,8 );
  SetSpritePattern( 10, semaphore_pattern_3,8 );
  SetSpritePattern( 11, semaphore_pattern_4,8 );

  SC5SpriteColors(1,LineColorsLayer04);
  SC5SpriteColors(2,LineColorsLayer14);
  SC5SpriteColors(3,LineColorsLayer12);
  SC5SpriteColors(4,LineColorsLayer12);

  Sprite16();
  
  // Printing initial coordinates on the Screen
  PutText(0,10,"SCREEN 5",0);  
    
  //sprintf(strx, "%i", xlefobj);
  Itoa (xlefobj,strx,10);
  //sprintf(stry, "%i", ylefobj);
  Itoa (ylefobj,stry,10);
  
  PutText(83,10,"(",0);
  PutText(91,10,strx,0);
  PutText(113,10,",",0);
  PutText(122,10,stry,0);
  PutText(144,10,")",0);

  //sprintf(strx, "%i", xrigobj);
  Itoa (xrigobj,strx,10);
  //sprintf(stry, "%i", yrigobj);
  Itoa (yrigobj,strx,10);
  
  PutText(160,10,"(",0);
  PutText(168,10,strx,0);
  PutText(190,10,",",0);
  PutText(199,10,stry,0);
  PutText(221,10,")",0);

  PutText(0,160,"HW COLISION",0);
  PutText(150,160,"SW COLISION",0);


  // Positioning initial Sprites on the Screen
  PutSprite (1,0,xlefobj,ylefobj,1);
  PutSprite (2,4,xrigobj,yrigobj,1);
  PutSprite (3,8,0,170,1);
  PutSprite (4,8,200,170,1);

  key = 0;
  // Game loop
  while (key!=27)
  {  
    stick = JoystickRead(0);
    key = Inkey();
        
    //sprintf(strx, "%i", xlefobj);
    Itoa (xlefobj,strx,10);
    //sprintf(stry, "%i", ylefobj);
    Itoa (ylefobj,stry,10);
    
    PutText(83,10,"(",0);
    PutText(91,10,strx,0);
    PutText(113,10,",",0);
    PutText(122,10,stry,0);
    PutText(144,10,")",0);        

    //sprintf(strx, "%i", xrigobj);
    Itoa (xrigobj,strx,10);
    //sprintf(stry, "%i", yrigobj);
    Itoa (yrigobj,stry,10);


    PutText(160,10,"(",0);
    PutText(168,10,strx,0);
    PutText(190,10,",",0);
    PutText(199,10,stry,0);
    PutText(221,10,")",0);
      
    // Up
    if(stick==1)
    {        
      ylefobj=(ylefobj-1);

      PutSprite (1,0,xlefobj,ylefobj,1);
    }

    // w
    if(key==119)
    {        
      yrigobj=(yrigobj-1);

      PutSprite (2,4,xrigobj,yrigobj,1);
    }

    // Right
    if(stick==3)
    {
      
      xlefobj=(xlefobj+1);
      //xlefobj=screen_limits_x(xlefobj);

      PutSprite (1,0,xlefobj,ylefobj,1);
    }

    // d
    if(key==100)
    {
      
      xrigobj=(xrigobj+1);
      //xrigobj=screen_limits_x(xrigobj);

      PutSprite (2,4,xrigobj,yrigobj,1);
    }

    // Down
    if(stick==5)
    {        
      ylefobj=(ylefobj+1);

      PutSprite (1,0,xlefobj,ylefobj,1);
    }

    // s
    if(key==115)
    {        
      yrigobj=(yrigobj+1);

      PutSprite (2,4,xrigobj,yrigobj,1);
    }

    // Left
    if(stick==7)
    {

      xlefobj=(xlefobj-1);
      //xlefobj=screen_limits_x(xlefobj);

      PutSprite (1,0,xlefobj,ylefobj,1);
    }

    // a
    if(key==97)
    {

      xrigobj=(xrigobj-1);
      //xrigobj=screen_limits_x(xrigobj);

      PutSprite (2,4,xrigobj,yrigobj,1);
    }

    // Hardware collision
    if (SpriteCollision()) {

      PutText(230,10,"1",0);
      SC5SpriteColors(3,LineColorsLayer13);
    } else {

      PutText(230,10," ",0);
      SC5SpriteColors(3,LineColorsLayer12);
    }

    // Software collision
    if ((abs(xlefobj - xrigobj) < 32) && (abs(ylefobj - yrigobj) < 32) ) {

      PutText(230,10,"2",0);
      SC5SpriteColors(4,LineColorsLayer13);
    } else {

      PutText(230,10," ",0);
      SC5SpriteColors(4,LineColorsLayer12);
    }

    FT_Wait(1);
  }

  Screen(0);
  Exit(0);
}
