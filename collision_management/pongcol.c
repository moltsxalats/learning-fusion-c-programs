//Documentation section

//
// pongcol.c
// Fusion-C Based the last Pong game example article
// Here we will add sofware collisions instead of hadware ones
// MoltSXalats 2024
//


// Preprocessor section

#include "fusion-c/header/msx_fusion.h"
#include "fusion-c/header/vdp_sprites.h"
#include "fusion-c/header/vdp_graph2.h"
#include <stdlib.h>


// Definition section of macros and symbolic constants

#define TRUE 1
#define FALSE 0


// Type definition section

typedef struct { 
	char spr;	// Sprite ID
    char y;		// Y destination of the Sprite
    char x;		// X destination of the Sprite
    char pat;	// Pattern number to use
    char col;	// Color to use (Not usable with MSX2's sprites)
} Sprite;


// Definition section of function prototypes

void GameStart(void);
void DrawSprite(void);
void BallCal(void);
void ComputerCal(void);
void AxisCal(void);
void FT_Wait(int);


// Definition section of global variables

// Direction of the ball
signed char DirX;
signed char DirY; 

char PlyScore,CpuScore;

// Definining the Ball, CPU Pad and Player Pad Structures
Sprite TheBall;
Sprite CpuPad;
Sprite PlyPad;

const char PatternBall[]={
						0b11100000,
						0b11100000,
						0b11100000,
						0b00000000,
						0b00000000,
						0b00000000,
						0b00000000,
						0b00000000
};

const char PatternPad[]={
						0b11100000,
						0b11100000,
						0b11100000,
						0b11100000,
						0b11100000,
						0b11100000,
						0b11100000,
						0b11100000
};

// Movement direction control by the cursors positions
const signed char moves[9]={0,-1,0,0,0,1,0,0,0}; 


// Section of the main function and the game loop

void main (void)
{
	
	char joy;


	SetColors(15,0,0);

	// 256 x 212 pixel
	Screen(5);
	Sprite8();
	SpriteDouble();
	KeySound(0);

	SetSpritePattern(0, PatternBall,8);
	SetSpritePattern(1, PatternPad,8);

	// Defining Variables

	// Player Pad Sprite initialization
	PlyPad.x=15;
	PlyPad.y=100;
	PlyPad.spr=1;
	PlyPad.pat=1;

	PlyScore=0;

	// Cpu Pad Sprite initialization
	CpuPad.x=240;
	CpuPad.y=100;
	CpuPad.spr=2;
	CpuPad.pat=1;

	CpuScore=0;

	// Ball Sprite initialization
	TheBall.x=128;
	TheBall.y=100;
	TheBall.spr=0;
	TheBall.pat=0;

	DirX=1;
	DirY=1;

	// IF MSX is Turbo-R Switch CPU to Z80 Mode
	if(ReadMSXtype()==3) {      
       ChangeCPU(0);    
   	}

	GameStart();

	// Main loop

	while (Inkey()!=27)
	{

		// Reading Joystick 0 (Keyboard)
		joy=JoystickRead(0);

		// Update the Y position of the Player Pad
		PlyPad.y+=moves[joy];
		BallCal();
		ComputerCal();
		DrawSprite();
		//AxisCal();
		FT_Wait(200);
	}

	// Ending program, and return to DOS
	Screen(0);
	KeySound(1);
	Exit(0);
}


// User defined functions section


// Print the initial game screen

void GameStart(void)
{
	char Temp[5];

	PutText((256-20*8)/2,4,"Press SPACE to START",0);

	// Initial Positions
	PlyPad.x=15;
	PlyPad.y=100;
	CpuPad.x=240;
	CpuPad.y=100;
	TheBall.x=128;
	TheBall.y=100;

	DirX*=-1;

	DrawSprite();

	Itoa(PlyScore,Temp,10);
	PutText(10,4,Temp,0);

	Itoa(CpuScore,Temp,10);
	PutText(235,4,Temp,0);	

	while (!IsSpace(WaitKey()))
	{}

	PutText((256-20*8)/2,4,"                    ",0);
}

// Put all sprite on screen

void DrawSprite(void)
{
		// Collision Detection

		// Collision with CPU Pad or Player Pad Sptite

		// If the ball's x < 128 we will look at the collision with the left paddle
	    if ( (TheBall.x <= 128) && (PlyPad.x + 6 >= TheBall.x) && (PlyPad.x <= TheBall.x + 6) && (PlyPad.y + 16 >= TheBall.y) && (PlyPad.y <= TheBall.y + 6) ) {
	      DirX*=-1;
	    }

	    // If the ball's x > 128 we will look at the collision with the right paddle
	   	if ( (TheBall.x > 128) && (TheBall.x + 6 >= CpuPad.x) && (TheBall.x <= CpuPad.x + 16) && (TheBall.y + 6 >= CpuPad.y) && (TheBall.y <= CpuPad.y + 16) ) {
	      DirX*=-1;
	    }


		PutSprite(PlyPad.spr,PlyPad.pat,PlyPad.x,PlyPad.y,15);
		PutSprite(CpuPad.spr,CpuPad.pat,CpuPad.x,CpuPad.y,15);
		PutSprite(TheBall.spr,TheBall.pat,TheBall.x,TheBall.y,15);

		// Check Ball Outside Game field
		if (TheBall.x<15)
		{
			CpuScore++;
			GameStart();
		}
		if (TheBall.x>245)
		{
			PlyScore++;
			GameStart();
		}

		// Check PlyPad Outside Game field
		if (PlyPad.y<10)
		{
			PlyPad.y=10;			
		}
		if (PlyPad.y>190)
		{
			PlyPad.y=190;
		}
}

// Ball Position 

void BallCal(void)
{
	TheBall.x+=DirX;
	TheBall.y+=DirY;

	// Douncing the Ball on the Top and Bottom border 
	if (TheBall.y>=190 || TheBall.y<5)		
		DirY*=-1;
}

// Simple Algorythm ! Cpu Cannot be beaten !

void ComputerCal(void)
{
		CpuPad.y=TheBall.y;				
}

// Prints Ball Coordinates

void AxisCal(void)
{
	char Temp[5];

	Itoa(TheBall.x,Temp,10);
	if(StrLen(Temp)==1)
	{	
		if(TheBall.x==9)
			PutText(100,4,"   ",0);
		PutText(100,4,Temp,0);
	}

	if(StrLen(Temp)==2) {
		if(TheBall.x==99)
			PutText(100,4,"   ",0);
		PutText(100,4,Temp,0);
	}

	if(StrLen(Temp)==3)
	{
		PutText(100,4,Temp,0);
	}


	Itoa(TheBall.y,Temp,10);
	if(StrLen(Temp)==1)
	{
		if(TheBall.y==9)			
			PutText(140,4,"   ",0);
		PutText(140,4,Temp,0);
	}

	if(StrLen(Temp)==2) {
		if(TheBall.y==99)
			PutText(140,4,"   ",0);
		PutText(140,4,Temp,0);
	}

	if(StrLen(Temp)==3)
	{
		PutText(140,4,Temp,0);
	}
}

// Wait Routine

void FT_Wait(int cicles) {
  unsigned int i;

  for(i=0;i<cicles;i++)
    {}
}
