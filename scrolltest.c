/* Programa per crear l'animació final i les pantalles de les lletres */
// L'A és el 65
#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include <stdio.h>
#include <string.h>

#define HALT __asm halt __endasm // wait for the next interrupt

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char  LDbuffer[BUFFER_SIZE_SC5];


char mypalette_3[16 * 4];
char mypalette_2[16 * 4];
char mypalette_1[16 * 4];

void WAIT(int cicles) {
  int i;
  for (i = 0; i < cicles; i++)
    HALT;
  return;
}

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(6,0,0);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        
    {
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

        return(1);
}

int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}



/* void CallMain_asm()__naked { */
/*   __asm */
/*     call _animacio_pantalla_inicial */
/*     ret */
/*   __endasm; */
/* } */

void main() {
  // Carreguem música que ja ho farà el programa principal
  Screen(0);
  SetColors(6, 12, 12);

  Screen(5);

  PutText(10,10,"Loading images\n\r",0);

  FT_LoadSc5Image("Final_1.sc7", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); 
  FT_LoadPalette("Final_1.pl7", LDbuffer, mypalette_1);
  FT_LoadSc5Image("Final_2.sc7", 512, LDbuffer, 512,
                  BUFFER_SIZE_SC5); 
  FT_LoadPalette("Final_2.pl7", LDbuffer, mypalette_2);
  FT_LoadSc5Image("Final_3.sc7", 768, LDbuffer, 512,
                  BUFFER_SIZE_SC5); 
  FT_LoadPalette("Final_3.pl7", LDbuffer, mypalette_3);

  SetScrollDouble(0);
  SetScrollMask(1);
  SetDisplayPage(2);
  SetActivePage(2);
  SetScrollH(125);
  SetScrollV(15);

  PutText(10, 10,
          "Next:\n\rScrollH 125px, ScrollV 15px\n\Mask enable 1 page\n\rActive page 2", 0);

  WaitKey();

  /* Ara amb màscara 0 */

  SetScrollMask(0);

  SetDisplayPage(0);
  SetActivePage(0);

  SetScrollH(0);
  SetScrollH(125);
  SetScrollV(15);

  PutText(10,10,"ScrollH 125px, ScrollV 15px\n\rMask disable 1 page\n\rActive page 0",0);
  WaitKey();

  /* Ara amb doble pàgina */
  SetScrollMask(0);
  SetScrollDouble(1);
  SetDisplayPage(1);
  SetActivePage(1);

  SetScrollH(0);
  SetScrollH(125);
  SetScrollV(80);

  PutText(10,10,"ScrollH 125px, ScrollV 80px\n\rMask disable 2 pages\n\rActive and visual 1\n\r",0);

  WaitKey();

  /* Ara amb doble pàgina */
  SetScrollMask(0);
  SetScrollDouble(1);
  SetDisplayPage(2);
  SetActivePage(2);

  SetScrollH(0);
  SetScrollH(75);
  SetScrollV(120);

  PutText(10,10,"ScrollH 75px, ScrollV 120px\n\rMask disable 2 pages\n\rActive and visual 2\n\r",0);

  WaitKey();

  /* Ara amb doble pàgina i la visual i activa és la 3 */
  SetScrollMask(0);
  SetScrollDouble(1);
  SetDisplayPage(3);
  SetActivePage(3);

  SetScrollH(0);
  SetScrollH(75);
  SetScrollV(120);

  PutText(10, 10,
          "ScrollH 75px, ScrollV 120px\n\rMask disable 2 pages\n\rActive and visual 3\n\r",
          0);

  WaitKey();

  /* Fem tot un scroll veient la part ocultable a veure si hi ha molt soroll */
  SetScrollMask(0);
  SetScrollDouble(1);
  SetDisplayPage(3);
  SetActivePage(3);

  for (int i = 0; i < 140; i++) {
    SetScrollH(i);
    WAIT(2);
  }

  SetScrollH(0);
  PutText(10, 10,
          "We've scrolled page 1 374 px step by step\n\rMask disable 2 pages\n\rActive and visual 1\n\r",
          0);

  WaitKey();

  /* Fem tot un scroll amagant la part ocultable a veure si hi ha molt soroll */
  SetScrollMask(1);
  SetScrollDouble(0);
  SetDisplayPage(1);
  SetActivePage(1);

  for (int i = 0; i < 140; i++) {
    SetScrollH(i);
    WAIT(2);
  }

  SetScrollH(374);
  PutText(10, 10,
          "We've scrolled page 1 374 px step by step\n\rMask disable 2 pages ",
          0);

  WaitKey();

  Screen(0);
  RestorePalette();
  Exit(0);
}
