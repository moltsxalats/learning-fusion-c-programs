import glob
import os

def task_tot_c():
    fitxers_c = glob.glob('*.c')
    for fitxerc in fitxers_c:
        arrel_fitxer = fitxerc[0:-2]
        yield {'name': 'compilem amb MBM: ' + fitxerc,
               'actions': [f'sdcc -V --code-loc 0x106 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ ./fusion-c/lib/crt0_msxdos.rel FM_MBM.rel {fitxerc}'],
               'file_dep': [fitxerc,'FM_MBM.rel','FM_MBM.h'],
               'targets': [arrel_fitxer+".ihx"]}
        yield {'name': 'passem a binari: '+ arrel_fitxer + '.com',
               'actions': [f'./Tools/Hex2Bin/vFusion13/hex2bin -e com {arrel_fitxer}.ihx',f'cp {arrel_fitxer}.com ./out/dska/'],
               'file_dep': [f'{arrel_fitxer}.ihx'],
               'targets': [f'{arrel_fitxer}.com']}

def task_learning_c():
    fitxers_c = glob.glob('learning-fusion-c-programs/*.c')
    for fitxerc in fitxers_c:
        arrel_fitxer = fitxerc[0:-2]
        separem = os.path.split(fitxerc)
        if fitxerc.find("_arg") >= 0:
            print(f'sdcc -V --code-loc 0x106 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ ./fusion-c/lib/crt0_msxdos_advanced.rel -o {separem[0]} {arrel_fitxer}')
            arrel_fitxer = arrel_fitxer.replace("_arg","")
            yield {'name': f'*** arg *** compilem del learning fusion amb arg: {arrel_fitxer} {fitxerc}',
                   'actions': [f'sdcc -V --code-loc 0x180 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ ./fusion-c/lib/crt0_msxdos_advanced.rel -o {arrel_fitxer.replace("_arg","")}.ihx {fitxerc}'],
                   'file_dep': [fitxerc],
                   'targets': [arrel_fitxer+".ihx"]}
        else:
            print(f'sdcc -V --code-loc 0x106 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ ./fusion-c/lib/crt0_msxdos_advanced.rel -o {separem[0]} {fitxerc}')
            yield {'name': 'compilem del learning fusion: ' + fitxerc,
                   'actions': [f'sdcc -V --code-loc 0x106 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ ./fusion-c/lib/crt0_msxdos.rel -o {separem[0]}/ {fitxerc}'],
                'file_dep': [fitxerc],
                   'targets': [arrel_fitxer+".ihx"]}
        # Passem ihx a com
        yield {'name': 'passem a binari del learning fusion: '+ arrel_fitxer + '.com',
               'actions': [f'./Tools/Hex2Bin/vFusion13/hex2bin -e com {arrel_fitxer}.ihx',f'cp {arrel_fitxer}.com ./out/dska/'],
               'file_dep': [f'{arrel_fitxer}.ihx'],
               'targets': [f'{arrel_fitxer}.com']}


def task_tot_c_mod():
    fitxers_c_mod = glob.glob('*.c_mod')
    for fitxerc in fitxers_c_mod:
        arrel_fitxer = fitxerc[0:-6]
        yield {'name': 'compilem ' + arrel_fitxer,
               'actions': [f'sdcc -V --code-loc 0x8000 --data-loc 0x0 --disable-warning 196 -mz80 --no-std-crt0 --opt-code-size fusion-DOS.lib -L ./fusion-c/lib/ -I ./fusion-c/include/ -x c {fitxerc}'],
               'targets': [arrel_fitxer+".rel"],
               'file_dep': [f'{fitxerc}']
               }
        yield {'name': 'generem el ' + arrel_fitxer + '.bin',
               'actions': [f'./Tools/Hex2Bin/vFusion13/hex2bin {arrel_fitxer}.ihx',f'cp {arrel_fitxer}.bin ./out/dska/{arrel_fitxer}.bMo'],
               'file_dep': [f'{arrel_fitxer}.rel'],
               'targets': [f'./out/dska/{arrel_fitxer}.bMo']}
        # Sembla que ja no faci falta el dd, ja l'ha compilat sense offset ¿?
