/* Un cop ja tinc els patrons amb el xoc_fons, vull provar de fer un scroll utilitzant el scroll hardware, a veure si amb un rètol petit a baix n'hi ha prou
 */

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <stdio.h>
#include <string.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];
unsigned char map_tile_x; // Per indicar la rajola de dalt a l'esquerra. Potser es pot fusionar amb tile_esq. No es pot fusionar, ja que tile_esq és la posició actual del personatge
unsigned char map_tile_y;
unsigned char x, y;
unsigned char debugar=0;

#define ACT_KEY_A 64 // Línia 2
#define ACT_KEY_D 2  // Línia 3
#define ACT_KEY_W 16 // Línia 5
#define ACT_KEY_S 1 // Línia 5
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_1 256
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_2 256 * 2
// A on comença la pàgina 2 de l'screen 5. Comtpador començant per 0
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_3 256 * 3
// A on comença la pàgina 3 de l'screen 5. Comtpador començant per 0
#define NOMBRE_RAJOLES_HOR 64
// Nombre de rajoles que conté el mapa
#define NOMBRE_RAJOLES_VER 64
#define NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS 32 // Rajoles a pintar en una linia horitzontal
#define NOMBRE_RAJOLES_PANTALLA_HOR 31
#define NOMBRE_RAJOLES_PANTALLA_VER 28 // Aquestes són les que es veuen en pantalla
#define NOMBRE_RAJOLES_PANTALLA_VER_SCROLL 32 // Aquesta són les de tota la pàgina que es fa l'scroll
// En total només visualitzo 31, ja que la 32 queda amagada quan es fa l'scroll. Però el faré més curt, de 26. Controlar les altres 5ho trobava complicat quan es barrejava amb scroll vertical

// L'he fet de 50 columnes però per tema de simplificar càlculs hauria de ser potència de 2
const char map1[] = {
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,0,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,32,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,5,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,64,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,96,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,3,4,3,64,65,66,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,64,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,64,65,66,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,0,2,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,32,34,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,64,66,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,
35,36,96,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36
};

static const unsigned char sprite[] = {
  0b00111000,
  0b01000100,
  0b00101000,
  0b00010000,
  0b00111000,
  0b01111100,
  0b01111110,
  0b11111111
};

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); 
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}
char copsVsync;
char processar_moviment;

void main_loop(void) {
    if (copsVsync > 0) {
        copsVsync = 0;
        processar_moviment = 1;
    }
    copsVsync++;
}


int stamp_x; // Les coordenades a on està la imatge del tile a transformar. Hauré de fer una funció i un lookup table
int stamp_y; // Mantindran les coordenades a on està el tipus de tile

void obtenir_coordenades_rajola(char map_x, char map_y) {
  // Li passem les coordenades del mapa i retorna la posició que s'ha de retallar per fer el HMMC (stamp_x, stamp_y)
  char tipus = map1[map_x + (map_y * NOMBRE_RAJOLES_HOR)];
  stamp_x = (tipus % NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS) * 8;
  stamp_y = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + (tipus / NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS)*8;
}
void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("PatCit.sc5", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette("PatCit.pl5", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  // Fem un escombrat del mapa per pintar cada patró
  // Ara el mapa del joc és més gran que la pantalla. He d'escombrar diferent
  for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER; m++) {
    for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
      // És fins al 31, ja que els últims 8 pixels queden amagats en fer
      // l'scroll. Són els últims 6 pixels
      obtenir_coordenades_rajola(n + map_tile_x, m + map_tile_y);
      HMMM(stamp_x, stamp_y, n * 8,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + m * 8, 8, 8);
    }
  }

  // SpriteDouble();
  SpriteSmall();
  SetSpritePattern(0, sprite, 8);
  char colorSprites[] = {1, 9, 10, 1, 9, 10, 7, 13};
  SetSpriteColors(0, colorSprites);
  // Resetegem la resta d'sprites, estaven tots a la línia 217 quan s'inicialitzava l'aplicació
  for(int k=1; k<32; k++){
    PutSprite(k,k,255,255,0);
  }
}

char pos_scroll_x;
char pos_scroll_y;
char fer_scroll_lateral; // Per si ja hem arribat als límits
char fer_scroll_vertical;
char desti_x;
char desti_y;
void scroll_amunt() {
  if(map_tile_y >= 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y -=1;

    if((pos_scroll_y & 7)==0) {
      map_tile_y -=1;
      desti_y = pos_scroll_y - 8;
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x, map_tile_y);
        desti_x = (n + (pos_scroll_x>>3)) * 8; 
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y -= 1; // Amb aquesta comanda i la de sota ja ho fa tot bé el de pintar. Abans hi havia un error quan era en aquest llindar
    }
    SetScrollV(pos_scroll_y);
  }
  if(map_tile_y == 0 && fer_scroll_vertical==1){
    fer_scroll_vertical = 0;
    pos_scroll_y -= 1;
  }
}

void scroll_avall() {
  if (map_tile_y < NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER - 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y += 1;

    if ((pos_scroll_y & 7) == 0) {
      map_tile_y += 1;
      desti_y = pos_scroll_y + 216;  // Per què 216, d'on surt???
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x,
                                   map_tile_y + NOMBRE_RAJOLES_PANTALLA_VER - 1);
        desti_x = (n + (pos_scroll_x>>3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y += 1;
    }
    SetScrollV(pos_scroll_y);
  }
  if ((map_tile_y == NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER + 1) && fer_scroll_vertical==1) {
    // He de sumar a NOMBRE_RAJOLES_PANTALLA_VER perquè. També hem de tenir en compte que queda a mitges. S'haurà de pintar el bordó
    fer_scroll_vertical = 0;
    pos_scroll_y += 1;
  }
}


void scroll_esq() {
  if (map_tile_x>3) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1) {
    pos_scroll_x -= 1; // És un char quan arribi a 0 i se li resti 1 torna a 256

    if ((pos_scroll_x & 7) == 0) {
      map_tile_x -= 1; // És un char, no arriba mai a ser més petit que 0
      // Falta la segona part que queda tallada entre les dues pàgines
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256
      /* for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y; m++) { */
      /*   obtenir_coordenades_rajola(map_tile_x, m + map_tile_y); */
      /*   HMMM(stamp_x, stamp_y, desti_x, */
      /*        OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + (m+desti_y) * 8, 8, 8); */
      /* } */
      /* for (int m = NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y; */
      /*      m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL; m++) { */
      /*   obtenir_coordenades_rajola(map_tile_x , m + map_tile_y); */
      /*   HMMM(stamp_x, stamp_y, desti_x, */
      /*        OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + */
      /*            (m - NOMBRE_RAJOLES_PANTALLA_VER_SCROLL + desti_y) * 8, 8, 8); */
      /* } */

      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL ; m++) {
        obtenir_coordenades_rajola(map_tile_x, m + map_tile_y);
        desti_y = (m + (pos_scroll_y >> 3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }

      pos_scroll_x -=1; // Sembla que amb aquest extra de desplaçament les col·lisions cap a l'esquerra després del primer xoc funcionen. Però si faig la seqüència de pintar, encara la fa malament
    }
    SetScrollH(pos_scroll_x);
  }
  if (map_tile_x == 3 && fer_scroll_lateral == 1) {
    // Per què ha de ser 3? Pot ser menor
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x -=1; // Faltava aquest scroll per poder mantenir la lògica de les caselles
  }
}

void scroll_dreta() {
  if (map_tile_x < NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR - 1) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1){
    pos_scroll_x += 1;
    if ((pos_scroll_x & 7) == 0) {
      map_tile_x += 1; 
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256

      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL; m++) {
        obtenir_coordenades_rajola(map_tile_x + 31, m + map_tile_y);
        desti_y = (m + (pos_scroll_y >> 3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }

      pos_scroll_x +=1;
    }
    SetScrollH(pos_scroll_x);
  }
  if (map_tile_x == (NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR -1) && fer_scroll_lateral == 1) {
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x +=1 ; 
  }
}

char stick;
char space;
char x,y;
char strx[6], stry[6];
char es_casellaParet(int nombre_casella){
  if (map1[nombre_casella]<3) {
      return(1);
  }  else if (map1[nombre_casella]>=32 && map1[nombre_casella]<=34 ) {
    return(1);
  } else if (map1[nombre_casella] >= 64 && map1[nombre_casella] <= 66) {
    return (1);
  } else if (map1[nombre_casella] >= 96 && map1[nombre_casella] <= 98) {
    return (1);
  } else {
    return(0);
  }
}

char tile_esq;
char tile_amunt;
int casella_1;
int casella_2;
void calculem_tiles(){
  // Investiguem caselles a on estem de la pantalla. El scroll i el mapa ja s'ajusten amb el map_tile_x
  tile_esq = (x + (pos_scroll_x & 7)) >> 3;
  tile_amunt = (y + (pos_scroll_y&7))>>3;
}
void moviment_amunt(){
  y = (y - 1);
  calculem_tiles();
  // Mirem si la casella d'abaix a l'esquerra és una casella (tile) de paret
  // He de mirar tant el de la meva esquerra tile_esq, més el de la meva dreta
  // que és la part del cos de la dreta tile_esq+1
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y + 1;
  }
  if (y < 14) {
    y = y + 1;
    scroll_amunt();
  }
}
void moviment_avall(){
  y = (y + 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y - 1;
  }
  if (y > 206 - 8) {
    y = y - 1;
    scroll_avall();
  }
}
void moviment_dreta(){
  x = (x + 1);
  calculem_tiles();
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x - 1;
  }
  if (x > 241) {
    // Els primers estan amagats
    x = x - 1;
    scroll_dreta();
  }
}
void moviment_esquerra(){
  x = (x - 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x + 1;
  }
  if (x < 14) {
    // Els primers estan amagats
    x = x + 1;
    scroll_esq();
  }
}
void moviment_sprite(){
  while (Inkey()!=27)
  {
    if (processar_moviment == 1) {
      stick = JoystickRead(0);
      space = TriggerRead(0);

      if (stick == 1) {
        // Amunt
        moviment_amunt();
      }
      else if (stick == 2) {
        // Amunt dreta
        moviment_dreta();
        moviment_amunt();
      } else if (stick == 3) {
        // Dreta
        moviment_dreta();
      } else if (stick == 4) {
        // Avall Dreta
        moviment_dreta();
        moviment_avall();
      } else if (stick == 5) {
        // Avall
        moviment_avall();
      } else if (stick == 6) {
        // Avall Esquerra
        moviment_esquerra();
        moviment_avall();
      } else if (stick == 7) {
        // Esquerra
        moviment_esquerra();
      } else if (stick == 8) {
        // Amunt Esquerra
        moviment_esquerra();
        moviment_amunt();
      }
      PutSprite(0, 0, x, y+pos_scroll_y, 4); // Sumem el pos_scroll_y, ja que hi ha un offset cada cop que fem scroll
      processar_moviment = 0;
    }
  }
}

void main(){
  Screen(5);
  SetDisplayPage(2);
  map_tile_x = 12;
  map_tile_y = 14;
  init_pantalla_joc();
  // Canviem el color 15 perquè sigui blanc i el text
  SetColorPalette(15 , 7, 7, 7);
  copsVsync = 0;
  processar_moviment=0;
  pos_scroll_x = 1;
  pos_scroll_y = 1; // L'inicialitzem amb 1 perquè així si vaig amunt també el detecta com a 0. 
  fer_scroll_lateral = 1;
  fer_scroll_vertical = 1;
  SetScrollMask(1);
  SetScrollH(1); // Comencem a la mateixa posició que el que hem après de scrpro.c

  InitializeMyInterruptHandler((int)main_loop, 1);
  // Posem sprite al 50,50
  x=20; y=19;
  PutSprite(0, 0, x, y, 4);
  moviment_sprite();
  EndMyInterruptHandler();
  Screen(0);
  RestorePalette();
  Locate(0, 0);
  printf("Pos sprite: %d, %d",x,y);
  Locate(0,1);

  printf("Tiles: %d, %d; %d-%d; %d-%d", tile_esq, tile_amunt,
         casella_1, casella_2,
         es_casellaParet(casella_1), es_casellaParet(casella_2));
  Locate(0,2);
  printf("scrl_x:%d;scrl_y;%d;", pos_scroll_x, pos_scroll_y);
  Locate(0, 3);
  printf("mptle_x:%d;frscrlat:%d;", map_tile_x, fer_scroll_lateral);
  Locate(0, 4);
  printf("mptle_y:%d;frscrver:%d;", map_tile_y, fer_scroll_vertical);
  Locate(0, 5);
  printf("scroll_x + x: %d; >>3: %d\n\r", pos_scroll_x + x,
         ((x + pos_scroll_x) & 255) >> 3);
  printf("desti: %d, %d\n\r", desti_x, desti_y);
  printf("Debugar: %d", debugar);
  Exit(0);
}

// La seqüència de map_tile_x, map_tile_y: c e; c d; c c; b c; a c; Fa que pinti el mapa següent 8 caselles més avall. Els moviments són: amunt, amunt, esquerra, esquerra. Quan surt dels dos d'amunt és quan deixa d'anar bé.
// He provat i anant cap a la dreta no ho fa. Quina serà la diferència?
// És si el canvi es produeix juts al límit, just quan ha canviat de valor el map_tile_x, si després faig una posició més amunt de l'sprite i faig el mateix moviment ja funciona.
// Fent només amunt i esquerra ja surten desplaçats cap avall una casella
/* He fet el seguiment dels passos per fer l'error i de les variables:
        map_tile_x	map_tile_y	desti_x	desti_y
        0x3949	0x394a	0x3969	0x396a
Petant quan vaig just amunt I esquerra
amunt fins break	c	d	f8	f8
esquerra fins break	b	d	f8	0 // Hauria de continuar sent f8. Qui el posa a 0? És el byt 1aac que és la línia 447, la variable m=0
esquerra fins break ja surt malament	a	d	f0	0
Sense petar
amunt fins break	c	d	f8	f8
amunt una mica abans break	c	d	f8	f8
esquerra fins break	b	d	f8	1f
esquerra fins break surt bé	a	d	f0	1f

A la línia 447 ja ha posat el 0. És en el càlcul desti_y que posa el 0. L'està calculant malament. És un valor de pos_scroll_y que fa que posi el 0. He d'estudiar com evoluciona aquesta variable
*/

// Amb l'anterior seqüència es dibuixa malament el mapa. Però si en començar ja anem al primer a l'esquerra fins que canvia map_tile_x i anem a la dreta, ja no detecta bé les col·lisions. Ara bé, si avancem una mica a l'esquerra abans de tornar a canviar el tile, ja ho fa bé

// 2024-02-17 Quan vaig de dreta a esquerra no falla, però de dreta a esquerra després el pinta malament. Sense haver pujat res, només scroll lateral
// He posat el mateix offset a la dreta i ja no hi ha l'error. Queda veure el d'amunt i avall
// Només un amunt fins al limit i baixant ja els dibuixa malament. Provaré de fer el mateix que amb la dreta i esquerra
// En posar la mateixa solució que per l'scroll horitzontal, pos_scroll_y++ deixa totalment de trobar les col·lisions amb el terreny. Realment sorprenent
// He de debugar amb els tiles a veure què passa a la frontera que es destirota tot.

// 2024-02-18 Mirant el calculant tiles de passar-li un paràmetre diferent
// He decidit d'arreglar l'scroll afegint +1 i -1 en el vertical tal i com es fa en l'horitzontal i ja no crea el caos. Ara bé, totes les col·lisions amb el fons es perden. Miraré d'arreglar-les de nou.
// Amb els canvis de l'scroll, sense fer cap scroll, és a dir, sense haver entrat al bucle, ja no detecta bé les col·lisions cap amunt, totes les altres les detecta bé.
// Debugar map_tile_* i tile_amunt tile_esq

// 2024-02-19 Sembla que calcula el tile_amunt malament. Mirar de posar watchpoint a l'escriptura i mirar quan es destirota
// He fet el càlcul de la suma fora del char i ja detecta bé les col·lisions sense scroll. Si vaig dreta i esquerra també ho fa bé. Quan vaig cap amunt fent l'scroll és quan ja es destirota tot de nou

// 2024-02-21 He descarregat l'antic que feia bé les col·lisions a veure què
// passa. Jo diria que no fa bé la suma en el càlcul del tile_amunt. He fet
// l'operació en python de (y+pos_scroll_y()>>3) + 32*cops_tot_scrollvertical - cops_augmentat_amp_y_scrollvertical i ha coincidit amb el no peta, la línia 29 de l'excel de debugant. He de fer el mateix amb el que peta
// No sé com el sdcc tracta el char o int d'un byte. Segons gemini sempre com a char a no sé que s'especific signed char

// 2024-05-02 Després de penjar l'article i fer el vídeo, he vist que no pinta bé quan fa els laterals. Si pujo i baixo fa bé els obstacles. Si vaig dreta i esquerra també. Ara bé, després d'haver anat avall i després a l'esquerra, falla. Sembla que sigui els que s'han quedat a mitges, ja que després si desapareixen, els tornar a pintar bé. Els antics sí que els està calculant bé, però els nous que dibuixa els dibuixa malament
// 2024-05-05 Sembla que sigui una part del pintar. La meitat la dibuixa bé, però l'altra meitat no ho fa
