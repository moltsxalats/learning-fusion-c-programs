/* Oduvaldo indica que per tenir les dues fucnions diferents en les dues interrupcions està fet en el Fusion 1.3 amb dues interrupcions diferents que es diuen InitInterrupt and InitVdpInterrupt
 Passaré a fer les proves amb la interrupció de línia i punt */

// Farem proves amb les inerrupcions de línia partint de la idea de
// https://msxpen.com/codes/-NAAaPeqw-Y8T3PlyQLC
/* ; ------------------------ set up interrupt ----------------------------- */

/*             ;130 ' Let's disable interrups until we are ready (DI) */
/*             ;140 '#I 243 */
/*             di */
            
/*             ;150 ' We will use general interrupt hook in address &hFD9A (-614) */
/*             ;160 ' This address is called every time when interrupt occurs. */
/*             ;170 ' To use it, we have to copy line 80 to the hook. */
/*             ;180 ' First we need to know in what memory address line 80 is: */
/*             ;190 ' AD = LINE 80 (NOTE: RENUM does not work in this case!!!) */
/*             ;200 '#I 33,@80,34,AD */

/* 			;db	0x21 ; =decimal 33 : LD HL, nn nn */
/*             ;dw	.line_80 */
/*             ;db	0x22 ; =decimal 34 : LD (nn nn), HL */
/*             ;dw  Var_AD */

/*             ;210 ' ... and then just copy... */
/* 			; FOR I=0 TO 4:POKE -614+I,PEEK(AD+I):NEXT I */

/* 			ld 		a, 0xc3    ; 0xc3 is the opcode for "jp", so this sets "jp .line_80" as the interrupt code */
/*             ld 		(0xfd9a), a */
/*             ld 		hl, .line_80 */
/*             ld 		(0xfd9a + 1), hl */

            

/*             ; ' We want to have line interrupts, so let's enable them. */
/*             ; VDP(0)=VDP(0) OR 16 */
/* 			ld  	a, (REG0SAV) */
/*             or  	16 */
/* 			ld  	b, a		; data to write */
/*             ld  	c, 0		; register number (9 to 24	Control registers 8 to 23	Read / Write	MSX2 and higher) */
/*             call  	WRTVDP_without_DI_EI		; Write B value to C register */
/*             ;di */

/*             ; ' Let's set the interrupt to happen on line 100 */
/*             ; VDP(20)=100 */
/* 			ld  	b, 100		; data to write */
/*             ld  	c, 19		; register number (9 to 24	Control registers 8 to 23	Read / Write	MSX2 and higher) */
/*             call  	WRTVDP_without_DI_EI		; Write B value to C register */
/*             ;di */

/*             ; ' Now we are ready and we can enable interrupts (EI) */
/*             ; '#I 251 */
/*             ei */


/***********************************/
// He vist que el codi de l'Oduvaldo permet fer també l'altre hook. Ho provaré

/* També he vist que aquí està ben explicat com funciona les interrupcions de línia i perquè quan detecta una a vegades no detecta l'altra, ja que en llegir es posen els bits a 0: https://www.msx.org/forum/msx-talk/development/line-interrupt-in-assembly */
/* Aquest no va, el que funciona és intliscr.c */


#include "../fusion-c/include/msx_fusion.h"
#include <stdio.h>
#include <string.h>

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}

char copsVsync;
char copsLinia;
char numLinia;

char cpt;
char Ystart = 10;
char Yend = 60;
char Y = 0;
signed char d = -1;

// This routine is called when Hsync is detected
void HBlankHook(void) {

  // First Hsync
  if (cpt == 0) {

    VDPwriteNi(19, Y + Ystart); // Set the second Hsync to another line
    SetColors(15, 9, 8);
    cpt++;

  } else {
    // Second Hsync detected
    VDPwriteNi(19, Y + Yend); // Set the next Hsync to another line (Here, the
                              // same as the first Hsync)
    SetColors(15, 5, 7);
    cpt = 0;
  }
}

void main_loop(void) {
  if (IsHsync()) {
    HBlankHook();
    copsLinia ++;
  }
  else {
    copsVsync++;
  }
}

// Wait routine
void FT_Wait(int N_cycles) {
  unsigned int i;

  for (i = 0; i < N_cycles; i++) {
    while (Vsynch() == 0) {
    }
  }
}

void main(){
  char IE1;

  copsVsync = 0;
  copsLinia = 0;

  Screen(5);

  // Activem interrupció de línia
  VDPwriteNi(19, 50);
  IE1 = Peek(0xF3DF) | 0b00010000; // RG0SAVE Adreça escriptura del VDP del registr0, que és el 0xF3DF (el reg0save)
  Poke(0xF3DF, IE1);               // Save New Value
  VDPwriteNi(0, IE1);              // Enable HSYNCH in REG0

  InitializeMyInterruptHandler((int)main_loop, 0); // Si en lloc de posar-la al genèric la poso al VDP no funciona tampoc.

  // Potser el problema era el WaitKey que estava dins les funcions de crida
  while (Inkey() != 27) {
    FT_Wait(500);
    Y = Y + d;
    if (Y > 50 || Y < 0) {
      d = d * -1;
    }
  }

  IE1 = Peek(0xF3DF) & 0b11101111; // Disable the Hsynch Hook
  Poke(0xF3DF, IE1);
  VDPwriteNi(0, IE1);

  EndMyInterruptHandler();

  printf("Valor VDP %d, linia %d\n\r", copsVsync, copsLinia);
  Screen(0);
  Exit(0);
}

