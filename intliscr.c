/* Un cop ha funcionat el intlin, mirem de fer-ho nosaltres per screen 5 amb scroll, semblant al que feia el codi de MSX Pen */

/* També he vist que aquí està ben explicat com funciona les interrupcions de
 * línia i perquè quan detecta una a vegades no detecta l'altra, ja que en
 * llegir es posen els bits a 0:
 * https://www.msx.org/forum/msx-talk/development/line-interrupt-in-assembly */

/***********************************/
#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include <stdio.h>
#include <string.h>

/***** CARREGUEM IMATGES ***********/
static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];

char mypalette_3[16 * 4];
char mypalette_2[16 * 4];
char mypalette_1[16 * 4];

void FT_SetName( FCB *p_fcb, const char *p_name )  // Routine servant à vérifier le format du nom de fichier
{
  char i, j;
  memset( p_fcb, 0, sizeof(FCB) );
  for( i = 0; i < 11; i++ ) {
    p_fcb->name[i] = ' ';
  }
  for( i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++ ) {
    p_fcb->name[i] =  p_name[i];
  }
  if( p_name[i] == '.' ) {
    i++;
    for( j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.'); j++ ) {
      p_fcb->ext[j] =  p_name[i + j] ;
    }
  }
}


void FT_errorHandler(char n, char *name)            // Gère les erreurs
{
  Screen(0);
  SetColors(6,0,0);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}
 
int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        
    {
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file  
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

        return(1);
}

int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_openFile(char *file_name) {
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }
}

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}

char copsVsync;
char copsHsync;
char processar_moviment;

__at 0xC002 char cpt;
__at 0xc000 char pos_scroll_x;
__at 0xc001 char pos_scroll_y;
__at 0xc004 char stick;

#define Ystart 189
#define Yend  220

/* Ystart = 205-16; */
/* Yend = 208; */
#define Y 0
signed char d = -1;

void canvi_pagina(char numPag) __naked
{
  // Extret de SetDisplayPage però traient els di i ei
  __asm
    ld iy,#2
    add iy,sp ;Bypass return address of the function
    ld a,(iy) ; x
    	rla
	rla
	rla
	rla
	rla
	and #0x7F
	or #0x1F
	ld b,a 
	ld a,#2
	or #0x80
	ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), b			;;	out data
	out		(c), a			;;	out VDP register number
	ld (#0xFAF5),a
	ret
  __endasm;
}

unsigned char VDPstatusSenseInt(unsigned char registerNumber) __naked {
  // Extret de vpstatusni.s. Sembla que el sufix ni és per indicar que no hi ha interrupcions
  // El HSync es troba a l'include msx-fusion.h i està definit com #define IsHsync() ((VDPstatusNi( 1 ) & 0x01) != 0)
  __asm
	ld		iy, #2
	add		iy, sp

    ld		a, (iy)		;;	port
	and		#0x0F

	ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), a			;;	status register number
	ld		a, #0x8F		;;	VDP register R#15
	out		(c), a			;;	out VDP register number
	in		l, (c)			;;	read VDP S#n
	xor		a
	out		(c), a
	ld		a, #0x8F		;;	VDP register R#15
	out		(c), a			;;	out VDP register number
	ret
  __endasm;

}

// This routine is called when Hsync is detected
// Fa glitch al line_interrupt = 192 i vertical_scroll = 3. Un mes avall o mes amunt i no fa glitch
// He de determinar si és HSync sense activar les interrupcions.
void HBlankHook(void) {
  if (cpt == 0) {
    // Línia Yend. Final interrupció
    //SetScrollV(pos_scroll_y);
    VDPwriteNi(23, pos_scroll_y); // He tret el posar l'scroll i ja no hi ha glitch, però fa coses rares l'scroll
    //SetDisplayPage(2);
    //canvi_pagina(0);
    VDPwriteNi(2, 0x1F);
    //VDPwriteNi(2, 0b01000000);
    VDPwriteNi(19, Y + Ystart + pos_scroll_y); // Set the second Hsync to another line
    // Les primeres vegades no passa. Mirar l'article del Grauw
    VDPstatusSenseInt(1);
    cpt++;
  } else {
    // Línia Ystart. Inici d'interrupció
    // Second Hsync detected
    // SetScrollV(0);
    // SetDisplayPage(0);
    //VDPwriteNi(2,100);
    //canvi_pagina(1);
    VDPwriteNi(2,0x3f);
    VDPwriteNi(23, 0); // El problema és aquest scroll que fa que el comportament no sigui l'esperat, deu recalcular totes les línies per tornar a calcular la interrupció
    VDPwriteNi(19, Y + Yend   ); // Sempre hi havia posat aquí també un pos_scroll_y però feia que s'anés aixamplant la zona immòbil. És curiós que aquí no s'hagi de posar l'offset. En Grauw comenta alguna cosa a la part final del seu article https://map.grauw.nl/articles/split_guide.php
    VDPstatusSenseInt(1);
    cpt = 0;
  }

  copsHsync++;
}

void main_loop(void) {    if (copsVsync > 0) {
        processar_moviment = 1;
    }
    copsVsync++;
    unsigned char status = VDPstatusSenseInt(1);
    status = status & 0x01;
    if (status != 0) { // amb IsHsync() ja funcionava correctament
      HBlankHook();
    }
}


// Wait routine
void FT_Wait(int N_cycles) {
  unsigned int i;

  for (i = 0; i < N_cycles; i++) {
    while (Vsynch() == 0) {
    }
  }
}

void main(){
  char IE1;

  copsVsync = 0;
  copsHsync = 0;
  pos_scroll_x = 0;
  pos_scroll_y = 0;
  cpt = 0;

  Screen(5);

  SetDisplayPage(1);
  FT_LoadSc5Image("Final_1.sc7", 0, LDbuffer, 256, BUFFER_SIZE_SC5);
  FT_LoadPalette("Final_1.pl7", LDbuffer, mypalette_1);
  FT_LoadSc5Image("imadeb1.s05", 256, LDbuffer, 256, BUFFER_SIZE_SC5);

  
  // Activem interrupció de línia
  VDPwriteNi(19, 50);
  IE1 = Peek(0xF3DF) | 0b00010000; // RG0SAVE Adreça escriptura del VDP del registr0, que és el 0xF3DF (el reg0save)
  Poke(0xF3DF, IE1);               // Save New Value
  VDPwriteNi(0, IE1);              // Enable HSYNCH in REG0

  InitializeMyInterruptHandler((int)main_loop, 0); // Si en lloc de posar-la al genèric la poso al VDP no funciona tampoc.

  // Potser el problema era el WaitKey que estava dins les funcions de crida
  while (Inkey() != 27) {
    if(processar_moviment == 1) {
      stick = JoystickRead(0);
      if (stick == 1) {
        if (pos_scroll_y == 0xfb) {
          pos_scroll_y = 0xfd;
        } else {
          pos_scroll_y++;
        }
      }
      if (stick == 5) {
        if (pos_scroll_y == 0xfd) {
          pos_scroll_y = 0xfb;
        } else {
          pos_scroll_y--;
        }
      }
      if (stick != 0) {
        stick = 0;
      }
      processar_moviment = 0;
    }
  }

  IE1 = Peek(0xF3DF) & 0b11101111; // Disable the Hsynch Hook
  Poke(0xF3DF, IE1);
  VDPwriteNi(0, IE1);

  EndMyInterruptHandler();

  SetScrollV(0);
  Screen(0);
  printf("Valor VDP %d, linia %d\n\r", copsVsync, copsHsync);
  Exit(0);
}

/*
  Extret del mame: https://github.com/mamedev/mame/blob/master/src/devices/video/v9938.cpp
From: awulms@inter.nl.net (Alex Wulms)
*** About the HR/VR topic: this is how it works according to me:

*** HR:
HR is very straightforward:
-HR=1 during 'display time'
-HR=0 during 'horizontal border, horizontal retrace'
I have put 'display time' and 'horizontal border, horizontal retrace' between
quotes because HR does not only flip between 0 and 1 during the display of
the 192/212 display lines, but also during the vertical border and during the
vertical retrace.

*** VR:
VR is a little bit tricky
-VR always gets set to 0 when the VDP starts with display line 0
-VR gets set to 1 when the VDP reaches display line (192 if LN=0) or (212 if
LN=1)
-The VDP displays contents of VRAM as long as VR=0

As a consequence of this behaviour, it is possible to program the famous
overscan trick, where VRAM contents is shown in the borders:
Generate an interrupt at line 230 (or so) and on this interrupt: set LN=1
Generate an interrupt at line 200 (or so) and on this interrupt: set LN=0
Repeat the above two steps

*** The top/bottom border contents during overscan:
On screen 0:
1) The VDP keeps increasing the name table address pointer during bottom
border, vertical retrace and top border
2) The VDP resets the name table address pointer when the first display line
is reached

On the other screens:
1) The VDP keeps increasing the name table address pointer during the bottom
border
2) The VDP resets the name table address pointer such that the top border
contents connects up with the first display line. E.g., when the top border
is 26 lines high, the VDP will take:
'logical'      vram line
TOPB000  256-26
...
TOPB025  256-01
DISPL000 000
...
DISPL211 211
BOTB000  212
...
BOTB024  236



*** About the horizontal interrupt

All relevant definitions on a row:
-FH: Bit 0 of status register 1
-IE1: Bit 4 of mode register 0
-IL: Line number in mode register 19
-DL: The line that the VDP is going to display (corrected for vertical scroll)
-IRQ: Interrupt request line of VDP to Z80

At the *start* of every new line (display, bottom border, part of vertical
display), the VDP does:
-FH = (FH && IE1) || (IL==DL)

After reading of status register 1 by the CPU, the VDP does:
-FH = 0

Furthermore, the following is true all the time:
-IRQ = FH && IE1

The resulting behaviour:
When IE1=0:
-FH will be set as soon as display of line IL starts
-FH will be reset as soon as status register 1 is read
-FH will be reset as soon as the next display line is reached

When IE=1:
-FH and IRQ will be set as soon as display line IL is reached
-FH and IRQ will be reset as soon as status register 1 is read

Another subtile result:
If, while FH and IRQ are set, IE1 gets reset, the next happens:
-IRQ is reset immediately (since IRQ is always FH && IE1)
-FH will be reset as soon as display of the next line starts (unless the next
line is line IL)

*** About the vertical interrupt:
Another relevant definition:
-FV: Bit 7 of status register 0
-IE0: Bit 5 of mode register 1

I only know for sure the behaviour when IE0=1:
-FV and IRQ will be set as soon as VR changes from 0 to 1
-FV and IRQ will be reset as soon as status register 0 is read

A consequence is that NO vertical interrupts will be generated during the
overscan trick, described in the VR section above.

I do not know the behaviour of FV when IE0=0. That is the part that I still
have to test.
*/

/* https://msx.org/forum/msx-talk/development/fusion-c-and-htimi?page=1 */

