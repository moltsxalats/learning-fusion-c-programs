/* Ja tinc l'scroll de fons que funciona, ara vull provar de posar aquí la interrupció de línia que havia fet al programa intliscr.c
 */

// Per debugar 0x99 $::wp_last_value != 143
#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <stdio.h>
#include <string.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];
unsigned char map_tile_x; // Per indicar la rajola de dalt a l'esquerra. Potser es pot fusionar amb tile_esq. No es pot fusionar, ja que tile_esq és la posició actual del personatge
unsigned char map_tile_y;
unsigned char x, y;
__at 0xC004 unsigned char debugar=0;

#define ACT_KEY_A 64 // Línia 2
#define ACT_KEY_D 2  // Línia 3
#define ACT_KEY_W 16 // Línia 5
#define ACT_KEY_S 1 // Línia 5
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_1 256
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_2 256 * 2
// A on comença la pàgina 2 de l'screen 5. Comtpador començant per 0
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_3 256 * 3
// A on comença la pàgina 3 de l'screen 5. Comtpador començant per 0
#define NOMBRE_RAJOLES_HOR 64
// Nombre de rajoles que conté el mapa
#define NOMBRE_RAJOLES_VER 64
#define NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS 32 // Rajoles a pintar en una linia horitzontal
#define NOMBRE_RAJOLES_PANTALLA_HOR 31
#define NOMBRE_RAJOLES_PANTALLA_VER 28 // Aquestes són les que es veuen en pantalla
#define NOMBRE_RAJOLES_PANTALLA_VER_SCROLL 32 // Aquesta són les de tota la pàgina que es fa l'scroll
// En total només visualitzo 31, ja que la 32 queda amagada quan es fa l'scroll. Però el faré més curt, de 26. Controlar les altres 5ho trobava complicat quan es barrejava amb scroll vertical

#define POSY_COMENCASCROLL_AMUNT 14
#define POSY_COMENCASCROLL_AVALL 180

// L'he fet de 50 columnes però per tema de simplificar càlculs hauria de ser potència de 2
const char map1[] = {
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,0,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,32,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,5,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,64,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,96,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,3,4,3,64,65,66,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,64,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,64,65,66,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,0,2,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,32,34,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,64,66,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,
35,36,96,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36
};

static const unsigned char sprite[] = {
  0b00111000,
  0b01000100,
  0b00101000,
  0b00010000,
  0b00111000,
  0b01111100,
  0b01111110,
  0b11111111
};

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); 
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}
// Wait routine
void FT_Wait(int N_cycles) {
  unsigned int i;

  for (i = 0; i < N_cycles; i++) {
    while (Vsynch() == 0) {
    }
  }

}

char copsVsync;
char copsHsync;
char processar_moviment;

__at 0xC002 char cpt;
__at 0xc000 char pos_scroll_x;
__at 0xc001 char pos_scroll_y;
#define Ystart 205-16
#define Yend  240
#define scroll_pos_cartell 44

#define Y 0
signed char d = -1;

void canvi_pagina(char numPag) __naked
{
  // Extret de SetDisplayPage però traient els di i ei
  __asm
    ld iy,#2
    add iy,sp ;Bypass return address of the function
    ld a,(iy) ; x
    	rla
	rla
	rla
	rla
	rla
	and #0x7F
	or #0x1F
	ld b,a 
	ld a,#2
	or #0x80
	ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), b			;;	out data
	out		(c), a			;;	out VDP register number
	ld (#0xFAF5),a
	ret
  __endasm;
}

void set_scrollH_Ni(char n) __naked
{
  // Extret del setscrollh.s
  __asm
  ld iy,#2
  add iy,sp;
  ld l,(iy); n, el paràmetre

    ld a,#7
    sub a,l 
    and #0b00000111
    ld b,a 
    ld a,#27
    or #0x80
    ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), b			;;	out data
	out		(c), a			;;	out VDP register number

    rr h
    ld a,l 
    rra 
    srl a
    srl a     
    
    ld b,a 
    ld a,#26
    or #0x80
    ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), b			;;	out data
	out		(c), a			;;	out VDP register number
    ret
    __endasm;
}
unsigned char VDPstatusSenseInt(unsigned char registerNumber) __naked {
  // Extret de vpstatusni.s. Sembla que el sufix ni és per indicar que no hi ha interrupcions
  // El HSync es troba a l'include msx-fusion.h i està definit com #define IsHsync() ((VDPstatusNi( 1 ) & 0x01) != 0)
  __asm
	ld		iy, #2
	add		iy, sp

    ld		a, (iy)		;;	port
	and		#0x0F

	ld		c, #0x99		;;	VDP port #1 (unsupport "MSX1 adapter")
	out		(c), a			;;	status register number
	ld		a, #0x8F		;;	VDP register R#15
	out		(c), a			;;	out VDP register number
	in		l, (c)			;;	read VDP S#n1
	xor		a
	out		(c), a
	ld		a, #0x8F		;;	VDP register R#15
	out		(c), a			;;	out VDP register number
	ret
  __endasm;

}

FastVram2Vram fastVDP;

void fastVDP_Ni(FastVram2Vram *paramFastVDP)__naked {
// El punter de l'estructura ha d'estar a HL, que quan es crida la funció ja ho fa automàticament. Així és com està fet a la llibreria Fusion-C. L'estructura és la mateixa que la del Fusion-C
// La funció està treta del Grauw de la pàgina https://map.grauw.nl/sources/docopy.php
  // La segona vegada després ESC es penja. Provar la funció del Fusion-c
  __asm
  DoCopy:
    ld a,#32
    ; di He tret el disable perquè prefereixo que es salti la còpia de les línies i que es vegi un flickering de línia que no pas tot el flickering d'una pantalla que no s'ha mostrat
    out (#0x99),a
    ld a,#17+#128
    ; ei
    out (#0x99),a
    ld bc,#0x0f9B
VDPready:
    ld a,#2
    out (#0x99),a
    ld a, #128+#15
    out (#0x99),a
    in a,(#0x99)      ; loop if vdp not ready (CE)
    rrca
    jp c,VDPready
    outi            ; 15x OUTI
    outi            ; (faster than OTIR)
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    outi
    ret
  __endasm;
}
// This routine is called when Hsync is detected
// Fa glitch al line_interrupt = 192 i vertical_scroll = 3. Un mes avall o mes amunt i no fa glitch
// He de determinar si és HSync sense activar les interrupcions.
void HBlankHook(void) {
  /* while (VDPstatusSenseInt(2)&0x01 != 0) { */
  /*   debugar = 4; */
  /* } */
  // He vist que hi ha el VDPready que fa això d'esperar i no té ei
  if (cpt == 0) {
    // Línia Yend. Final interrupció
    //SetScrollV(pos_scroll_y);
    VDPwriteNi(23, pos_scroll_y); // He tret el posar l'scroll i ja no hi ha glitch, però fa coses rares l'scroll
    //SetDisplayPage(2);
    //canvi_pagina(2);
    VDPwriteNi(2, 0x5F);
    //VDPwriteNi(2, 0b01000000);
    VDPwriteNi(19, Y + Ystart + pos_scroll_y); // Set the second Hsync to another line
    // Les primeres vegades no passa. Mirar l'article del Grauw
    set_scrollH_Ni(pos_scroll_x);
    // Activar els sprites # R8 Mouse-optical-transparent-colorbus-VRAM type-0-Sprites-Color
    VDPwriteNi(8,0b00001000);
    //SpriteOn();
    VDPstatusSenseInt(1);
    cpt++;
  } else {
    // Línia Ystart. Inici d'interrupció. La part baixa de la pantalla
    // Second Hsync detected
    // SetScrollV(0);
    // SetDisplayPage(0);
    //VDPwriteNi(2,100);
    //canvi_pagina(0);
    
    // També he de posar el registre horitzontal a 0 en aquest tros
    set_scrollH_Ni(0);
    VDPwriteNi(2, 0x3F);
    VDPwriteNi(23, scroll_pos_cartell); // El problema és aquest scroll que fa que el comportament no sigui l'esperat, deu recalcular totes les línies per tornar a calcular la interrupció
    VDPwriteNi(19, Y + Yend + scroll_pos_cartell); // Sempre hi havia posat aquí també un pos_scroll_y però feia que s'anés aixamplant la zona immòbil. És curiós que aquí no s'hagi de posar l'offset. En Grauw comenta alguna cosa a la part final del seu article https://map.grauw.nl/articles/split_guide.php
    // Desactivar els sprites perquè no apareguin a aquesta zona
    VDPwriteNi(8,0b00001010);
    SpriteOff();
    VDPstatusSenseInt(1);
    cpt = 0;
  }

  copsHsync++;
}

void main_loop(void) {    if (copsVsync > 0) {
        processar_moviment = 1;
    }
    copsVsync++;
    unsigned char status = VDPstatusSenseInt(1);
    status = status & 0x01;
    if (status != 0) { // amb IsHsync() ja funcionava correctament
      HBlankHook();
    }
}


int stamp_x; // Les coordenades a on està la imatge del tile a transformar. Hauré de fer una funció i un lookup table
int stamp_y; // Mantindran les coordenades a on està el tipus de tile

void obtenir_coordenades_rajola(char map_x, char map_y) {
  // Li passem les coordenades del mapa i retorna la posició que s'ha de retallar per fer el HMMC (stamp_x, stamp_y)
  char tipus = map1[map_x + (map_y * NOMBRE_RAJOLES_HOR)];
  stamp_x = (tipus % NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS) * 8;
  stamp_y = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + (tipus / NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS)*8;
}
void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("PatCit.sc5", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette("PatCit.pl5", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  FT_LoadSc5Image("fondo.SC5",0, LDbuffer, 512, BUFFER_SIZE_SC5);

  // Fem un escombrat del mapa per pintar cada patró
  // Ara el mapa del joc és més gran que la pantalla. He d'escombrar diferent
  for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER; m++) {
    for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
      // És fins al 31, ja que els últims 8 pixels queden amagats en fer
      // l'scroll. Són els últims 6 pixels
      obtenir_coordenades_rajola(n + map_tile_x, m + map_tile_y);
      HMMM(stamp_x, stamp_y, n * 8,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + m * 8, 8, 8);
    }
  }

  // SpriteDouble();
  SpriteSmall();
  SetSpritePattern(0, sprite, 8);
  char colorSprites[] = {1, 9, 10, 1, 9, 10, 7, 13};
  SetSpriteColors(0, colorSprites);
  // Resetegem la resta d'sprites, estaven tots a la línia 217 quan s'inicialitzava l'aplicació
  for(int k=1; k<32; k++){
    PutSprite(k,k,255,255,0);
  }

  // Aquí el fastVDP amb EI i després sense EI (Ni)
  // Copiem linies que desapareixeran
  fastVDP.sx = 0;
  fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 193;
  fastVDP.dx = 0;
  fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1+232;
  fastVDP.nx = 0;
  fastVDP.ny = 2;
  fastVDP.col = 0;
  fastVDP.param = 0;
  fastVDP.cmd = opYMMM;
  VDPready();
  debugar = 1;
  fastVDP_Ni(&fastVDP);
  debugar = 2;

  // Pintem ralla negra
  fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 234;
  fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 193;
  VDPready();
  debugar = 3;
  fastVDP_Ni(&fastVDP);

  // Per debugar copio els colors de la pàgina
  /* HMMM(32,69, 0,OFFSET_COORDENADAY_PAGINA_ACTIVA_2+195, 200,10); */
  /* HMMM(32,79, 200,OFFSET_COORDENADAY_PAGINA_ACTIVA_2+195, 56,10); */
  // L'scroll horitzontal va tapant les dues línies, no les torno a pintar, per això apareix fent pampallugues, borra la part de la recta que ja està fet.
}

char fer_scroll_lateral; // Per si ja hem arribat als límits
char fer_scroll_vertical;
char desti_x;
char desti_y;
void scroll_amunt() {
  if(map_tile_y >= 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y -=1;
    debugar = 1;
    // Retornem les línies borrades a la posició negre
    YMMM(0,OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 232, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + (int)(193 + pos_scroll_y),2);
    // Tornem a les línies negres
    YMMM(0,OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 234, OFFSET_COORDENADAY_PAGINA_ACTIVA_2+ (int)(193 + pos_scroll_y),2);
    if((pos_scroll_y & 7)==0) {
      map_tile_y -=1;
      desti_y = pos_scroll_y - 8;
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x, map_tile_y);
        desti_x = (n + (pos_scroll_x>>3)) * 8; 
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y -= 1;
    }
    // SetScrollV(pos_scroll_y);
  }
  if(map_tile_y == 0 && fer_scroll_vertical==1){
    fer_scroll_vertical = 0;
    pos_scroll_y -= 1;
  }
}

void scroll_avall() {
  if (map_tile_y < NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER - 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y += 1;
    // Retornem les línies borrades a la posició negre, reomplim pàgina scroll
    unsigned char nova_posicio = (191 + pos_scroll_y);
    if (nova_posicio == 255) {
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 232;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.ny = 1;
      VDPready();
      fastVDP_Ni(&fastVDP);
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 232 + 1;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2;
      VDPready();
      fastVDP_Ni(&fastVDP);
    } else {
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 232;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.ny = 2;
      VDPready();
      fastVDP_Ni(&fastVDP);
    }
    // Hem de fer el condicional quan està a la frontera del 254 perquè quedi partit, una línia i una altra
    if ((pos_scroll_y & 7) == 0) {
      map_tile_y += 1;
      desti_y = pos_scroll_y + 216;  // Per què 216, d'on surt???
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x,
                                   map_tile_y + NOMBRE_RAJOLES_PANTALLA_VER - 1);
        desti_x = (n + (pos_scroll_x>>3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y += 1;
    }
    nova_posicio = (192 + pos_scroll_y);
    if (nova_posicio == 255 ) {
      // Guardem les 2 següents línies, a la memòria, copiem linies que desapareixeran
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1+232;
      fastVDP.ny = 1;
      VDPready();
      fastVDP_Ni(&fastVDP);
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1+232+1;
      fastVDP.ny = 1;
      VDPready();
      fastVDP_Ni(&fastVDP);
      // Tornem a les línies negres
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 234;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.ny = 1;
      VDPready();
      fastVDP_Ni(&fastVDP);
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 234 + 1;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2;
      VDPready();
      fastVDP_Ni(&fastVDP);      
    } else {
      // Guardem les 2 següents línies, a la memòria, copiem linies que desapareixeran
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1+232;
      fastVDP.ny = 2;
      VDPready();
      fastVDP_Ni(&fastVDP);
      // Tornem a les línies negres
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 234;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + nova_posicio;
      fastVDP.ny = 2;
      VDPready();
      fastVDP_Ni(&fastVDP);
    }
  }
  if ((map_tile_y == NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER + 1) && fer_scroll_vertical==1) {
    // He de sumar a NOMBRE_RAJOLES_PANTALLA_VER perquè. També hem de tenir en compte que queda a mitges. S'haurà de pintar el bordó
    fer_scroll_vertical = 0;
    pos_scroll_y += 1;
  }
}


void scroll_esq() {
  if (map_tile_x>3) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1) {
    pos_scroll_x -= 1; // És un char quan arribi a 0 i se li resti 1 torna a 256

    if ((pos_scroll_x & 7) == 0) {
      map_tile_x -= 1; // És un char, no arriba mai a ser més petit que 0
      // Falta la segona part que queda tallada entre les dues pàgines
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256
      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL ; m++) {
        obtenir_coordenades_rajola(map_tile_x, m + map_tile_y);
        desti_y = (m + (pos_scroll_y >> 3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }

      pos_scroll_x -=1; // Sembla que amb aquest extra de desplaçament les col·lisions cap a l'esquerra després del primer xoc funcionen. Però si faig la seqüència de pintar, encara la fa malament
    }
    //SetScrollH(pos_scroll_x);
  }
  if (map_tile_x == 3 && fer_scroll_lateral == 1) {
    // Per què ha de ser 3? Pot ser menor
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x -=1; // Faltava aquest scroll per poder mantenir la lògica de les caselles
  }
}

void scroll_dreta() {
  if (map_tile_x < NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR - 1) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1){
    pos_scroll_x += 1;
    if ((pos_scroll_x & 7) == 0) {
      map_tile_x += 1; 
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256

      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL; m++) {
        obtenir_coordenades_rajola(map_tile_x + 31, m + map_tile_y);
        desti_y = (m + (pos_scroll_y >> 3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }

      pos_scroll_x +=1;
    }
    //SetScrollH(pos_scroll_x);
  }
  if (map_tile_x == (NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR -1) && fer_scroll_lateral == 1) {
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x +=1 ; 
  }
}

char stick;
char space;
char x,y;
char strx[6], stry[6];
char es_casellaParet(int nombre_casella){
  if (map1[nombre_casella]<3) {
      return(1);
  }  else if (map1[nombre_casella]>=32 && map1[nombre_casella]<=34 ) {
    return(1);
  } else if (map1[nombre_casella] >= 64 && map1[nombre_casella] <= 66) {
    return (1);
  } else if (map1[nombre_casella] >= 96 && map1[nombre_casella] <= 98) {
    return (1);
  } else {
    return(0);
  }
}

char tile_esq;
char tile_amunt;
int casella_1;
int casella_2;
void calculem_tiles(){
  // Investiguem caselles a on estem de la pantalla. El scroll i el mapa ja s'ajusten amb el map_tile_x
  tile_esq = (x + (pos_scroll_x & 7)) >> 3;
  tile_amunt = (y + (pos_scroll_y&7))>>3;
}
void moviment_amunt(){
  y = (y - 1);
  calculem_tiles();
  // Mirem si la casella d'abaix a l'esquerra és una casella (tile) de paret
  // He de mirar tant el de la meva esquerra tile_esq, més el de la meva dreta
  // que és la part del cos de la dreta tile_esq+1
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y + 1;
  }
  if (y < POSY_COMENCASCROLL_AMUNT) {
    y = y + 1;
    scroll_amunt();
  }
}
void moviment_avall(){
  y = (y + 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y - 1;
  }
  if (y > POSY_COMENCASCROLL_AVALL) {
    y = y - 1;
    scroll_avall();
  }
}
void moviment_dreta(){
  x = (x + 1);
  calculem_tiles();
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x - 1;
  }
  if (x > 241) {
    // Els primers estan amagats
    x = x - 1;
    scroll_dreta();
  }
}
void moviment_esquerra(){
  x = (x - 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x + 1;
  }
  if (x < 14) {
    // Els primers estan amagats
    x = x + 1;
    scroll_esq();
  }
}
void moviment_sprite(){
  while (Inkey()!=27)
  {
    if (processar_moviment == 1) {
      stick = JoystickRead(0);
      space = TriggerRead(0);

      if (stick == 1) {
        // Amunt
        moviment_amunt();
      }
      else if (stick == 2) {
        // Amunt dreta
        moviment_dreta();
        moviment_amunt();
      } else if (stick == 3) {
        // Dreta
        moviment_dreta();
      } else if (stick == 4) {
        // Avall Dreta
        moviment_dreta();
        moviment_avall();
      } else if (stick == 5) {
        // Avall
        moviment_avall();
      } else if (stick == 6) {
        // Avall Esquerra
        moviment_esquerra();
        moviment_avall();
      } else if (stick == 7) {
        // Esquerra
        moviment_esquerra();
      } else if (stick == 8) {
        // Amunt Esquerra
        moviment_esquerra();
        moviment_amunt();
      }
      PutSprite(0, 0, x, y+pos_scroll_y, 4); // Sumem el pos_scroll_y, ja que hi ha un offset cada cop que fem scroll
      processar_moviment = 0;
    }
  }
}

void main(){
  char IE1;
  
  Screen(5);
  SetDisplayPage(2);
  map_tile_x = 12;
  map_tile_y = 14;
  init_pantalla_joc();
  // Canviem el color 15 perquè sigui blanc i el text
  SetColorPalette(15 , 7, 7, 7);
  copsVsync = 0;
  copsHsync = 0;
  processar_moviment=0;
  cpt=0;
  pos_scroll_x = 1;
  pos_scroll_y = 1; // L'inicialitzem amb 1 perquè així si vaig amunt també el detecta com a 0. 
  fer_scroll_lateral = 1;
  fer_scroll_vertical = 1;
  SetScrollMask(1);
  SetScrollH(1); // Comencem a la mateixa posició que el que hem après de scrpro.c

  // Activem interrupció de línia
  VDPwriteNi(19, 50);
  IE1 = Peek(0xF3DF) | 0b00010000; // RG0SAVE Adreça escriptura del VDP del
                                   // registr0, que és el 0xF3DF (el reg0save)
  Poke(0xF3DF, IE1);               // Save New Value
  VDPwriteNi(0, IE1);              // Enable HSYNCH in REG0

  InitializeMyInterruptHandler((int)main_loop, 0);

  // Posem sprite al 50,50
  x=20; y=19;
  PutSprite(0, 0, x, y, 4);
  moviment_sprite();

  IE1 = Peek(0xF3DF) & 0b11101111; // Disable the Hsynch Hook
  Poke(0xF3DF, IE1);
  VDPwriteNi(0, IE1);

  EndMyInterruptHandler();
  Screen(0);
  RestorePalette();
  Locate(0, 0);
  printf("Pos sprite: %d, %d",x,y);
  Locate(0,1);

  printf("Tiles: %d, %d; %d-%d; %d-%d", tile_esq, tile_amunt,
         casella_1, casella_2,
         es_casellaParet(casella_1), es_casellaParet(casella_2));
  Locate(0,2);
  printf("scrl_x:%d;scrl_y;%d;", pos_scroll_x, pos_scroll_y);
  Locate(0, 3);
  printf("mptle_x:%d;frscrlat:%d;", map_tile_x, fer_scroll_lateral);
  Locate(0, 4);
  printf("mptle_y:%d;frscrver:%d;", map_tile_y, fer_scroll_vertical);
  Locate(0, 5);
  printf("scroll_x + x: %d; >>3: %d\n\r", pos_scroll_x + x,
         ((x + pos_scroll_x) & 255) >> 3);
  printf("desti: %d, %d\n\r", desti_x, desti_y);
  printf("Debugar: %d; %d; %d", debugar, copsHsync, copsVsync);
  Exit(0);
}

// 2024-10-21 Ho he reprès per veure què passava amb la línia d'interrupció. Al fitxer intliscr.c fa la interrupció correctamente, però aquí sembla que hi hagia algun problema amb l'scroll.
// Si HBlankHook no fa res, funciona correctament. Miraré només de fer un canvi de paleta a aquell tros de línia, sense fer res de l'scroll. A veurè què passa. Un canvi del color de fons com en el intlin
// He provat d'utilitzar el SetColorPalette i canvien tots els pixels de colors en l'openmsx
// Ni amb els colors dels marges tampoc ho fa bé com en el intLin.c No sé quina és la diferència
// 2024-10-22 He provat el IntLin i amb Screen(5) i sí que ha funcionat. He copiat el mateix aquí i també va canviant el color de fondo mentre fa l'scroll. Ara queda fer bé el càlcul per mantenir una zona que no es mogui
// 2025-02-27 Sembla que ja fa bé l'split line. Ara a copiar el rectangle per amagar les pampallugues de la línia a dibuixar. Ho guardarem les dues línies a la pàgina dels tiles
// 2025-03-10 Quan copiava el rectangle per provar només amb l'scroll avall, hi havia cops que feia pampallugues el line split, era perquè el YMMM fa un di i no es produïa la següent interrupció. He fet la comanda fastVDP perquè no faci el di. El problema és que es produeix una interrupció de línia quan s'està copiant les 2 línies negres i el mapa queda deformat. Potser el que hauria de fer és que quan estigui copiant les dues línies pugui saber si s'ha produït una interrupció de línia i així poder-les tornar a copiar. És possible? Poso just abans de copiar una variable a 0, si es dóna la interrupció sempre posa aquella variable a 1. Si me n'adono que ha canviat la torno a copiar i torno a fer el procés.
// També havia provat de posar el VDPready dins la interrupció i sempre era més llarga que 2 línies, sempre hi havia el flickering.
