/* PRograma en C per experimentar amb els patrons i la posició dels sprites.
 He utilitzat el primer món del Usas per veure com ho feien. Usen patrons de 8x8 pel que he pogut deduir la imatge.
Començarem carregant la imatge a VRAM*/

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <stdio.h>
#include <string.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];

const char map1[] = {
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,1,1,1, 1,1,1,1,1, 0,0,0,0,0, 1,1,1,1,1, 0,0,0,1,1, 0,0,1,1,1, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,

  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,

  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1,1,1,1,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1,1,1,1,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1,1,1,1,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,

  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,

  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
  0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 0,0,
};

static const unsigned char sprite[] = {
  0b00111000,
  0b01000100,
  0b00101000,
  0b00010000,
  0b00111000,
  0b01111100,
  0b01111110,
  0b11111111
};


void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

        return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); // Skip 0x29 first bytes of the file. És un binari tal i com diu el manual. Els 7 primers indiquen la posició de la memòria i els 22 següents és el codi
  /*
C000  ld     b,#10        06 10
C002  ld     d,#00        16 00
C004  ld     hl,#c022     21 22 C0  ;; És on comencen els bytes de la paleta
C007  ld     a,(hl)       7E
C008  sla    a            CB 27
C00A  sla    a            CB 27
C00C  sla    a            CB 27
C00E  sla    a            CB 27
C010  ld     c,a          4F
C011  inc    hl           23
C012  ld     e,(hl)       5E
C013  inc    hl           23
C014  ld     a,(hl)       7E
C015  add    a,c          81
C016  ld     ix,#014d     DD 21 4D 01
C01A  call   #015f        CD 5F 01   // Crida a una rutina de subrom http://map.tni.nl/resources/msxbios.php. La carregada a ix, en aquest 014d que segons la mateixa pàgina indica imprimir per impressora ¿¿???
C01D  inc    d            14
C01E  inc hl           23
C01F  djnz   #c007        10 E6
C021  ret                 C9
*/
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}
char copsVsync;
char processar_moviment;

void main_loop(void) {
    if (copsVsync > 2) {
        copsVsync = 0;
        processar_moviment = 1;
    }
    copsVsync++;
}

void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("UsasPatr.sc5", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette("UsasPatr.pl5", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  // Fem un escombrat del mapa per pintar cada patró
  for(int m=0;m<25;m++){
    for(int n=0;n<32;n++) {
      if (map1[n+m*32]==1) {
        HMMM(96,256,n*8,m*8,8,8);
      } else if (map1[n + m * 32] == 0) {
        HMMM(144, 256+120, n * 8, m * 8, 8, 8);
      }
    }
  }

  //SpriteDouble();
  SpriteSmall();
  SetSpritePattern(0, sprite, 8);
  char colorSprites[] = {1, 9, 10, 1, 9, 10, 7, 13};
  SetSpriteColors(0, colorSprites);
}
char stick;
char space;
char x,y;
char strx[6], stry[6];

void moviment_sprite(){
  while (Inkey()!=27)
  {
    if (processar_moviment == 1) {
      stick = JoystickRead(0);
      space = TriggerRead(0);

      sprintf(strx, "%i", x);
      sprintf(stry, "%i", y);

      PutText(50, 3, strx, 0);
      PutText(80, 3, stry, 0);

      if(stick==1)
      {        
        y=(y-1);
        // Investiguem caselles a on estem
        int tile_esq = x>>3;
        int tile_amunt = y>>3;
        // Mirem si la casella d'abaix a l'esquerra és una casella (tile) de paret
        // He de mirar tant el de la meva esquerra tile_esq, més el de la meva dreta que és la part del cos de la dreta tile_esq+1
        if (map1[tile_esq + 32 * (tile_amunt)] == 1 ||
            map1[tile_esq +1 + 32 * (tile_amunt)] == 1) {
          y=y+1;
        }
        PutSprite (0,0,x,y,4);
      }

      if(stick==3)
      {
        x=(x+1);
        int tile_esq = x >> 3;
        int tile_amunt = y >> 3;
        if (map1[tile_esq +1+ 32 * (tile_amunt+1)] == 1 ||
            map1[tile_esq+1 + 32 * (tile_amunt)] == 1) {
          x = x - 1;
        }
        PutSprite(0, 0, x, y, 4);
      }

      if(stick==5)
      {        
        y=(y+1);
        PutSprite(0, 0, x, y, 4);
        int tile_esq = x >> 3;
        int tile_amunt = y >> 3;
        if (map1[tile_esq + 32 * (tile_amunt+1)] == 1 ||
            map1[tile_esq + 1 + 32 * (tile_amunt+1)] == 1) {
          y = y - 1;
        }
      }

      if (stick == 7) {
        x = (x - 1);
        int tile_esq = x >> 3;
        int tile_amunt = y >> 3;
        if (map1[tile_esq + 32 * (tile_amunt)] == 1 ||
            map1[tile_esq + 32 * (tile_amunt+1)] == 1) {
          x = x + 1;
        }
        PutSprite(0, 0, x, y, 4);
      }
      processar_moviment = 0;
    }
  }
}

void main(){
  Screen(5);
  init_pantalla_joc();

  SetColorPalette(15, 7, 7, 7);
  // Canviem el color 15 perquè sigui blanc i el text
  copsVsync = 0;
  processar_moviment=0;
  InitializeMyInterruptHandler((int)main_loop, 1);
  // Posem sprite al 50,50
  x=50; y=50;
  PutSprite(0, 0, x, y, 4);
  moviment_sprite();
  EndMyInterruptHandler();
  Screen(0);
  RestorePalette();
  Exit(0);
}
