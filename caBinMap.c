/* Apliació que carrega diferents binaris de 16k en diferents pàgines per poder practicar l'assignació de memòria del DOS2 */
/* Programa basat en l'exemple de Fusion-c am-mapper.c */

#include <stdlib.h>
#include <stdio.h>
#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/rammapper.h"
#include <string.h>

static FCB file; // Inicialitzem l'estructura del sistema de fitxers

#define TAMANYBUFFER 1024*16
unsigned char __at 0x8000 BufferPagina [TAMANYBUFFER]; // 16Kb és el tamany que tenen les
                                            // pàgines del z80

MAPPERINFOBLOCK *table; // Inicialitzem estructura del DOS que guarda les pàgines de memòria


void printRamMapperStatus() {
    printf("Mappers status: \r\n");
    printf("============== \r\n");  

    table = _GetRamMapperBaseTable(); // Llegeix directament de la meòria dins la funció rammaper.s 
    while (table->slot!=0) {
        printf("Slot #%x \r\n",table->slot);
        printf("16KB Segments #%x \r\n",table->number16KBSegments);
        printf("Free 16KB Segments #%x \r\n",table->numberFree16KBSegments);
        printf("Allocated System 16KB Segments #%x \r\n",table->numberAllocatedSystem16KBSegments);
        printf("User 16KB Segments #%x \r\n",table->numberUser16KBSegments);                              
        printf("========================================== \r\n");      
        table = table + 1;      //one record (8 bytes per record)
    }
}

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadBin(char *file_name)        // Charge les données d'un fichiers
{
  int rd=2560;
        
  FT_SetName( &file, file_name );
  if(fcb_open( &file ) != FCB_SUCCESS) 
    {
      FT_errorHandler(1, file_name);
      return (0);
    }


  rd = fcb_read( &file, BufferPagina, TAMANYBUFFER );  // Llegim els 16K
  
  return(1);
}

void main(void)
{ 

    unsigned char *p;
    SEGMENTSTATUS *status;
    unsigned char segmentId0,segmentId1,initialSegment;

    InitRamMapperInfo(4);
     Cls();  
    printRamMapperStatus();
    printf("\r\n\r\n***********Allocating 2 user blocks\r\n");
    WaitKey();
    Cls();

    status = AllocateSegment(0,0);
    segmentId0 = status->allocatedSegmentNumber;
    printf("+++++++++++++++++++++  Block allocated , segmentId=#%x slot=#%x error=#%x \r\n",segmentId0, status->slotAddressOfMapper, status->carryFlag);
    printf("\r\n\r\n");
    
    status = AllocateSegment(0,0);
    segmentId1 = status->allocatedSegmentNumber;
    printf("+++++++++++++++++++++  Block allocated , segmentId=#%x slot=#%x error=#%x \r\n",segmentId1, status->slotAddressOfMapper, status->carryFlag);
    printf("\r\n\r\n");

    printRamMapperStatus();
    WaitKey();
    Cls();
    
    p = (unsigned char *) 0x8000;
    initialSegment = Get_PN(2);
    
    printf("Before settings segments  #%x \r\n",initialSegment);
    printf("[0x8000]=#%x \r\n",p[0]); 


    printf("Setting segment #%x at page 2 (from 0x8000 to 0xBFFF)\r\n",segmentId0);
    Put_PN(2,segmentId0);       
    printf("[0x8000]=#%x \r\n",p[0]); 
    p[0]=0xdd;
    printf("[0x8000]=#%x \r\n",p[0]); 
    
    
    printf("GET_PN = #%x \r\n",Get_PN(2));    
    
    WaitKey();
    Cls();
    
    printf("Setting segment #%x at page 2 (from 0x8000 to 0xBFFF)\r\n",segmentId1);
    Put_PN(2,segmentId1);
    printf("[0x8000]=#%x \r\n",p[0]); 
    p[0]=0xaa;
    printf("[0x8000]=#%x \r\n",p[0]); 

    FT_LoadBin("16k_2.bin");
    printf("super jep [0x8000]=#%x \r\n",p[0]);
    printf("super jep [0x800A]=#%x \r\n",p[10]);

    printf("GET_PN = #%x \r\n",Get_PN(2));    

    WaitKey();
    Cls();
    
    printf("Setting segment #%x at page 2 (from 0x8000 to 0xBFFF)\r\n",segmentId0);
    Put_PN(2,segmentId0);       
    printf("[0x8000]=#%x \r\n",p[0]); 

    printf("GET_PN = #%x \r\n",Get_PN(2));

    // Vaig a provar de carregar el fitxer en aquest segment
    FT_LoadBin("16k_1.bin");
    printf("Setting segment #%x at page 2 (from 0x8000 to 0xBFFF)\r\n",segmentId1);
    Put_PN(2,segmentId1);       
    printf("[0x8000]=#%x \r\n",p[0]); 
    printf("[0x8005]=#%x \r\n",p[5]); 
    printf("*****\nSuper Jep\n******\n");
    printf("GET_PN = #%x \r\n",Get_PN(2));    
    WaitKey();

    printf("Restoring original segment at page 2 (from 0x8000 to 0xBFFF)\r\n",initialSegment);
    Put_PN(2,initialSegment);       
    printf("[0x8000]=#%x \r\n",p[0]); 

    printf("GET_PN = #%x \r\n",Get_PN(2));        
    
    WaitKey();
    Cls();
    
    printf("\r\n\r\n***********Freeing block #%x\r\n",segmentId0);
    status = FreeSegment(segmentId0,0);
    printf("+++++++++++++++++++++++++++  Block freed, error=#%x \r\n",status->carryFlag); 
    printf("\r\n\r\n***********Freeing block #%x\r\n",segmentId1);
    status = FreeSegment(segmentId1,0);
    printf("+++++++++++++++++++++++++++  Block freed, error=#%x \r\n",status->carryFlag);
    printRamMapperStatus();


    


}
