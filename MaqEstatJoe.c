/* Programa que utilitzaré per fer les proves de la màquina d'estats del Joe
   També vull veure com funciona el OR amb el Fusion-C
 */

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include "../fusion-c/include/pattern_transform.h"
#include "Sprites_Joe.h"
#include <stdio.h>

char processar_mov;

unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}

char compta_tics;
void game_clock(void) {
  if (processar_mov == 0 && compta_tics > 2) {
    processar_mov = 1;
    compta_tics = 0;
  }
  compta_tics++;
}

// Enumeration for the states
typedef enum {
  MIRANT_AMUNT_1,
  MIRANT_AMUNT_2,
  MIRANT_DRETA_1,
  MIRANT_DRETA_2,
  MIRANT_AVALL_1,
  MIRANT_AVALL_2,
  MIRANT_ESQUERRA_1,
  MIRANT_ESQUERRA_2,
  // Add other states if needed
  NUM_STATES // Keep track of the number of states
} State;

// Enumeration for the keys (inputs)
typedef enum {
  KEY_ESQUERRA,
  KEY_DRETA,
  KEY_AMUNT,
  KEY_AVALL,
  NUM_KEYS // Keep track of the number of keys
} Key;

typedef enum {
  JOY_REPOS,
  JOY_AMUNT,
  JOY_AMUNT_DRETA,
  JOY_DRETA,
  JOY_AVALL_DRETA,
  JOY_AVALL,
  JOY_AVALL_ESQUERRA,
  JOY_ESQUERRA,
  JOY_AMUNT_ESQUERRA
} Joy;

// Structure to define each state and its corresponding patterns
typedef struct {
    char pattern_1; // First pattern for the sprite
    char pattern_2; // Second pattern for the sprite
  char color_1; // Per saber també la paleta dels colors corresponents
  char color_2;
  // color_1 i pattern_1 aniran al patró més baix, no han de tenir el ICC a 1
} Sprite;

// Array of sprites for each state
Sprite state_sprites[NUM_STATES];

typedef struct {
  int posx;
  int posy;
  char estat;
} Personatge;

Personatge Joe;

// Function to print the current sprite patterns
void pinta_sprite() {
  PutSprite(0, state_sprites[Joe.estat].pattern_1, Joe.posx, Joe.posy, 0);
  PutSprite(1, state_sprites[Joe.estat].pattern_2, Joe.posx, Joe.posy, 0);
  // Els colors no va per Sprites, va pels plans
  SetSpriteColors(0, &index_mask[state_sprites[Joe.estat].color_1*16]);
  SetSpriteColors(1, &index_mask[state_sprites[Joe.estat].color_2<<4]);
}

// Transition table based on current state and key input
State state_transition[NUM_STATES][NUM_KEYS] = {
    [MIRANT_AMUNT_1] = {MIRANT_ESQUERRA_1, MIRANT_DRETA_1, MIRANT_AMUNT_2, MIRANT_AVALL_1},
    [MIRANT_AMUNT_2] = {MIRANT_ESQUERRA_2, MIRANT_DRETA_2, MIRANT_AMUNT_1, MIRANT_AVALL_2},
    [MIRANT_DRETA_1] = {MIRANT_ESQUERRA_1, MIRANT_DRETA_2, MIRANT_AMUNT_1, MIRANT_AVALL_1},
    [MIRANT_DRETA_2] = {MIRANT_ESQUERRA_2, MIRANT_DRETA_1, MIRANT_AMUNT_2, MIRANT_AVALL_2},
    [MIRANT_AVALL_1] = {MIRANT_ESQUERRA_1, MIRANT_DRETA_1, MIRANT_AMUNT_1, MIRANT_AVALL_2},
    [MIRANT_AVALL_2] = {MIRANT_ESQUERRA_2, MIRANT_DRETA_2, MIRANT_AMUNT_2, MIRANT_AVALL_1},
    [MIRANT_ESQUERRA_1] = {MIRANT_ESQUERRA_2, MIRANT_DRETA_1, MIRANT_AMUNT_1, MIRANT_AVALL_1},
    [MIRANT_ESQUERRA_2] = {MIRANT_ESQUERRA_1, MIRANT_DRETA_2, MIRANT_AMUNT_2, MIRANT_AVALL_2}
};

void moviment_esquerra(){
  Joe.posx -=1 ;
  if (Joe.posx <0) {
    Joe.posx +=1;
  }
  else {
    Joe.estat = state_transition[Joe.estat][KEY_ESQUERRA];
  }
}

void moviment_dreta() {
  Joe.posx += 1;
  if (Joe.posx > 240) {
    Joe.posx -= 1;
  } else {
    Joe.estat = state_transition[Joe.estat][KEY_DRETA];
  }
}

void moviment_avall() {
  Joe.posy += 1;
  if (Joe.posy > 195) {
    Joe.posy -= 1;
  } else {
    Joe.estat = state_transition[Joe.estat][KEY_AVALL];
  }
}

void moviment_amunt() {
  Joe.posy -= 1;
  if (Joe.posy < 0) {
    Joe.posy += 1;
  } else {
    Joe.estat = state_transition[Joe.estat][KEY_AMUNT];
  }
}

// Function to process the input and move to the next state
void process_input(Joy stick) {
  // Segons stick dir si puc x+1 o y-1
  switch (stick) {
  case JOY_AMUNT:
    moviment_amunt(); // és com la d'enemScFo. Calcula si hi ha moviment cap amunt i el nou estat
    break;
  case JOY_AMUNT_DRETA:
    moviment_amunt();
    moviment_dreta();
    break;
  case JOY_DRETA:
    moviment_dreta();
    break;
  case JOY_AVALL_DRETA:
    moviment_avall();
    moviment_dreta();
    break;
  case JOY_AVALL:
    moviment_avall();
    break;
  case JOY_AVALL_ESQUERRA:
    moviment_avall();
    moviment_esquerra();
    break;
  case JOY_ESQUERRA:
    moviment_esquerra();
    break;
  case JOY_AMUNT_ESQUERRA:
    if ((Joe.posx & 1) == 0) {
      moviment_amunt();
      moviment_esquerra();
    } else {
      moviment_esquerra();
      moviment_amunt();
    }
    break;
  }

 pinta_sprite();
}

void main() {
  Screen(5);
  processar_mov = 1;
  Sprite16();
  SpriteOn();

  SetColors(4, 5, 1);
  SetPalette((Palette*) paleta_bricks);

  char k;
  for (k = 0; k < 18; k++) {
    SetSpritePattern(4*k, Sprite32Bytes(&Sprites[k*16]), 32);
  }
  Pattern16FlipVram(0, 72, 0);
  Pattern16FlipVram(4, 76, 0);
  Pattern16FlipVram(8, 80, 0);
  Pattern16FlipVram(12, 84, 0);

  // Definim tots els patrons que tenim i els seus colors
  state_sprites[MIRANT_AMUNT_1].pattern_1 = 32;
  state_sprites[MIRANT_AMUNT_1].pattern_2 = 36;
  state_sprites[MIRANT_AMUNT_2].pattern_2 = 44;
  state_sprites[MIRANT_AMUNT_2].pattern_1 = 48;
  state_sprites[MIRANT_DRETA_1].pattern_1 = 72;
  state_sprites[MIRANT_DRETA_1].pattern_2 = 76;
  state_sprites[MIRANT_DRETA_2].pattern_1 = 80;
  state_sprites[MIRANT_DRETA_2].pattern_2 = 84;
  state_sprites[MIRANT_AVALL_1].pattern_2 = 36;
  state_sprites[MIRANT_AVALL_1].pattern_1 = 40;
  state_sprites[MIRANT_AVALL_2].pattern_1 = 52;
  state_sprites[MIRANT_AVALL_2].pattern_2 = 44;
  state_sprites[MIRANT_ESQUERRA_1].pattern_1 = 0;
  state_sprites[MIRANT_ESQUERRA_1].pattern_2 = 4;
  state_sprites[MIRANT_ESQUERRA_2].pattern_1 = 8;
  state_sprites[MIRANT_ESQUERRA_2].pattern_2 = 12;

  state_sprites[MIRANT_AMUNT_1].color_1 = 8;
  state_sprites[MIRANT_AMUNT_1].color_2 = 9;
  state_sprites[MIRANT_AMUNT_2].color_1 = 12;
  state_sprites[MIRANT_AMUNT_2].color_2 = 11;
  state_sprites[MIRANT_DRETA_1].color_1 = 0;
  state_sprites[MIRANT_DRETA_1].color_2 = 1;
  state_sprites[MIRANT_DRETA_2].color_1 = 2;
  state_sprites[MIRANT_DRETA_2].color_2 = 3;
  state_sprites[MIRANT_AVALL_1].color_1 = 10;
  state_sprites[MIRANT_AVALL_1].color_2 = 9;
  state_sprites[MIRANT_AVALL_2].color_1 = 13;
  state_sprites[MIRANT_AVALL_2].color_2 = 11;
  state_sprites[MIRANT_ESQUERRA_1].color_1 = 0;
  state_sprites[MIRANT_ESQUERRA_1].color_2 = 1;
  state_sprites[MIRANT_ESQUERRA_2].color_1 = 2;
  state_sprites[MIRANT_ESQUERRA_2].color_2 = 3;

  // Estat i posició inicial
  Joe.estat = MIRANT_AMUNT_2; // Starting state
  Joe.posx = 100;
  Joe.posy = 100;

  pinta_sprite();

  // Anem a muntar bé tots els estats i les paletes perquè es vegin ben creats i comprovar que no hi hagi errors
  // Mirant_amunt_1
  PutSprite(2, state_sprites[MIRANT_AMUNT_1].pattern_1, 0, 4, 0);
  PutSprite(3, state_sprites[MIRANT_AMUNT_1].pattern_2, 0, 4, 0);
  SetSpriteColors(2, &index_mask[16 * 8]);
  SetSpriteColors(3, &index_mask[16 * 9]);
  // Mirant_avall_1
  PutSprite(4, state_sprites[MIRANT_AVALL_1].pattern_1, 16, 102, 0);
  PutSprite(5, state_sprites[MIRANT_AVALL_1].pattern_2, 16, 102, 0);
  SetSpriteColors(5, &index_mask[16 * 9]);
  SetSpriteColors(4, &index_mask[16 * 10]);
  /* // Mirant_amunt_2 */
  PutSprite(7, state_sprites[MIRANT_AMUNT_2].pattern_1, 30, 140, 0);
  PutSprite(8, state_sprites[MIRANT_AMUNT_2].pattern_2, 30, 140, 0);
  SetSpriteColors(8, &index_mask[16 * 11]);
  SetSpriteColors(7, &index_mask[16 * 12]);
  // Mirant_avall_2
  PutSprite(9, state_sprites[MIRANT_AVALL_2].pattern_1, 46, 6, 0);
  PutSprite(10, state_sprites[MIRANT_AVALL_2].pattern_2, 46, 6, 0);
  SetSpriteColors(10, &index_mask[16 * 11]);
  SetSpriteColors(9, &index_mask[16 * 13]);
  // Mirant_dreta_1 
  PutSprite(11, 72, 60, 24, 0);
  PutSprite(12, 76, 60, 24, 0);
  SetSpriteColors(11, &index_mask[16 * 0]);
  SetSpriteColors(12, &index_mask[16 * 1]);
  // Mirant_dreta_2
  PutSprite(13, 80, 76, 32, 0);
  PutSprite(14, 84, 76, 32, 0);
  SetSpriteColors(13, &index_mask[16 * 2]);
  SetSpriteColors(14, &index_mask[16 * 3]);
  // Mirant_esquerra_1
  PutSprite(15, 0, 92, 48, 0);
  PutSprite(16, 4, 92, 48, 0);
  SetSpriteColors(15, &index_mask[16 * 0]);
  SetSpriteColors(16, &index_mask[16 * 1]);
  // Mirant_esquerra_2
  PutSprite(17, 8, 108, 60, 0);
  PutSprite(18, 12, 108, 60, 0);
  SetSpriteColors(17, &index_mask[16 * 2]);
  SetSpriteColors(18, &index_mask[16 * 3]);

  compta_tics = 0;
  InitializeMyInterruptHandler((int)game_clock,1);

  char stick;

  while (Inkey() != 27 ) {
    stick = JoystickRead(0);
    if (processar_mov == 1) {
      // Fem tots els càlculs de la màquina d'estats i dels moviments
      process_input(stick);
      processar_mov = 0;
    }
  }
  EndMyInterruptHandler();

  RestorePalette();
  Screen(0);

  Exit(0);
}

