/* Basat en l'scroll fons però creant un segon personatge que seguirà al primer
   Crearé un buffer de joystick apretats de 8 posicions i l'aniré incrementant. Faig una acció i matxaco amb la llegida.
   Què passa amb les diagonals quan es bloquegen? S'haurà de controlar per dir que no fa acció
 */

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <stdio.h>
#include <string.h>

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];
unsigned char map_tile_x; // Per indicar la rajola de dalt a l'esquerra. Potser es pot fusionar amb tile_esq
unsigned char map_tile_y;
unsigned char x, y, x2, y2;
char multipleVuit;
unsigned char punter_historic;
unsigned char historic_moviments[16];
unsigned char debugar=0;
char pos_scroll_x;
char pos_scroll_y;

FastVram2Vram fastVDP;

#define ACT_KEY_A 64 // Línia 2
#define ACT_KEY_D 2  // Línia 3
#define ACT_KEY_W 16 // Línia 5
#define ACT_KEY_S 1 // Línia 5
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_1 256
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_2 256 * 2
// A on comença la pàgina 2 de l'screen 5. Comtpador començant per 0
#define OFFSET_COORDENADAY_PAGINA_ACTIVA_3 256 * 3
// A on comença la pàgina 3 de l'screen 5. Comptador començant per 0
#define NOMBRE_RAJOLES_HOR 64
// Nombre de rajoles que conté el mapa
#define NOMBRE_RAJOLES_VER 64
#define NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS 32 // Rajoles a pintar en una linia horitzontal
#define NOMBRE_RAJOLES_PANTALLA_HOR 31
#define NOMBRE_RAJOLES_PANTALLA_VER 28 // Aquestes són les que es veuen en pantalla
#define NOMBRE_RAJOLES_PANTALLA_VER_SCROLL 32 // Aquesta són les de tota la pàgina que es fa l'scroll
// En total només visualitzo 31, ja que la 32 queda amagada quan es fa l'scroll. Però el faré més curt, de 26. Controlar les altres 5ho trobava complicat quan es barrejava amb scroll vertical

// L'he fet de 50 columnes però per tema de simplificar càlculs hauria de ser potència de 2
const char map1[] = {
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,0,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,32,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,5,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,64,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,96,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,3,4,3,64,65,66,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,97,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,64,65,66,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,5,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,96,97,98,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,64,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,96,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,6,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,0,1,2,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,32,33,34,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,64,65,66,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,5,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,0,1,2,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,32,33,34,3,4,3,4,3,4,3,4,5,4,3,4,3,6,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,0,1,2,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,64,65,66,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,32,33,34,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,96,97,98,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,0,2,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,
35,36,32,34,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,
3,4,64,66,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,0,1,2,3,4,3,4,3,4,3,4,3,4,
35,36,96,98,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,32,33,34,35,36,35,36,35,36,35,36,35,36,
3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,4,3,96,97,98,3,4,3,4,3,4,3,4,3,4,
35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36,35,36
};

static const unsigned char sprite[] = {
  0b00111000,
  0b01000100,
  0b00101000,
  0b00010000,
  0b00111000,
  0b01111100,
  0b01111110,
  0b11111111
};

static const unsigned char sprite2[] = {
  0b11000111,
  0b10111011,
  0b11010111,
  0b11101111,
  0b11000111,
  0b10000011,
  0b10000001,
  0b00000000
};

void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); 
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

/***** INTERRUPCIONS *****/
unsigned char OldHook[5];
unsigned char MyHook[5];
unsigned char IntFunc[5];
unsigned char TypeOfInt;
__at 0xFD9F unsigned char VdpIntHook[];
__at 0xFD9A unsigned char AllIntHook[];
__at 0xF344 unsigned char RAMAD3;

void InterruptHandlerHelper (void) __naked
{
__asm
    push af
    call _IntFunc
    pop af
    jp _OldHook
__endasm;
}

void InitializeMyInterruptHandler (int myInterruptHandlerFunction, unsigned char isVdpInterrupt)
{
    unsigned char ui;
    MyHook[0]=0xF7; //RST 30 is interslot call both with bios or dos
    MyHook[1]=RAMAD3; //Page 3 generally is not paged out and is the slot of the ram, so this should be good
    MyHook[2]=(unsigned char)((int)InterruptHandlerHelper&0xff);
    MyHook[3]=(unsigned char)(((int)InterruptHandlerHelper>>8)&0xff);
    MyHook[4]=0xC9;
    IntFunc[0]=0xCD; //CALL
    IntFunc[1]=(unsigned char)((int)myInterruptHandlerFunction&0xff);
    IntFunc[2]=(unsigned char)(((int)myInterruptHandlerFunction>>8)&0xff);
    IntFunc[3]=0xC9;
    TypeOfInt = isVdpInterrupt;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();
    if (isVdpInterrupt)
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=VdpIntHook[ui];
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=MyHook[ui];
    }
    else
    {
        for(ui=0;ui<5;ui++)
            OldHook[ui]=AllIntHook[ui];
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=MyHook[ui];
    }

    //Re-enable Interrupts
    EnableInterrupt();
}

void EndMyInterruptHandler (void)
{
    unsigned char ui;
    //Interrupts must be disabled so no one messes with what we are doing
    DisableInterrupt();

    if (TypeOfInt)
        for(ui=0;ui<5;ui++)
            VdpIntHook[ui]=OldHook[ui];
    else
        for(ui=0;ui<5;ui++)
            AllIntHook[ui]=OldHook[ui];

    //Re-enable Interrupts
    EnableInterrupt();

}
char copsVsync;
char processar_moviment;

void main_loop(void) {
    /* if (copsVsync > 2) { */
    /*     copsVsync = 0; */
    /*     //processar_moviment = 1; */
    /* } */
    processar_moviment=1;
    copsVsync++;
}


int stamp_x; // Les coordenades a on està la imatge del tile a transformar. Hauré de fer una funció i un lookup table
int stamp_y; // Mantindran les coordenades a on està el tipus de tile

void obtenir_coordenades_rajola(char map_x, char map_y) {
  // Li passem les coordenades del mapa i retorna la posició que s'ha de retallar per fer el HMMC (stamp_x, stamp_y)
  char tipus = map1[map_x + (map_y * NOMBRE_RAJOLES_HOR)];
  stamp_x = (tipus % NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS) * 8;
  stamp_y = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + (tipus / NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS)*8;
}

void init_pantalla_joc() {
  // Carreguem la imatge dels patrons
  FT_LoadSc5Image("PatCit.sc5", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette("PatCit.pl5", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);

  // Fem un escombrat del mapa per pintar cada patró
  // Ara el mapa del joc és més gran que la pantalla. He d'escombrar diferent
  for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER; m++) {
    for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
      // És fins al 31, ja que els últims 8 pixels queden amagats en fer
      // l'scroll. Són els últims 6 pixels
      obtenir_coordenades_rajola(n + map_tile_x, m + map_tile_y);
      HMMM(stamp_x, stamp_y, n * 8,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + m * 8, 8, 8);
    }
  }

  // SpriteDouble();
  SpriteSmall();
  SetSpritePattern(0, sprite, 8);
  char colorSprites[] = {1, 9, 10, 1, 9, 10, 7, 13};
  SetSpriteColors(0, colorSprites);
  SetSpritePattern(1, sprite2, 8);
  char colorSprites2[] = {13, 7, 10, 9, 1, 10, 9, 1};
  SetSpriteColors(1, colorSprites);
  // Resetegem la resta d'sprites, estaven tots a la línia 217 quan
  // s'inicialitzava l'aplicació Segons l'Eric, posant el de més avall posa els
  // altres. Suposo que també els hauré d'anar canviant de lloc a mesura que
  // l'scroll puja
  /* for(int k=2; k<32; k++){ */
  /*   PutSprite(k,k,255,255,0); */
  /* } */
  PutSprite(2, 2, 255, 255, 0);
  for (char k=0; k<16;k++) {
    historic_moviments[k]=3;
  }

  // Creem el buffer de les posicions que va esborrant el cartell d'un pixel
  // Dalt
  HMMM(80,OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y,255-94,OFFSET_COORDENADAY_PAGINA_ACTIVA_1+244,94,12);

  // Copiem el cartell a la part de dalt
  HMMM(6,OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242,80,OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y,94,12);
}

char fer_scroll_lateral; // Per si ja hem arribat als límits
char fer_scroll_vertical;
char desti_x;
char desti_y;
void scroll_amunt() {
  multipleVuit = 0;
  if(map_tile_y >= 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y -=1;

    if((pos_scroll_y & 7)==0) {
      map_tile_y -=1;
      desti_y = pos_scroll_y - 8;
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x, map_tile_y);
        desti_x = (n + (pos_scroll_x>>3)) * 8; // Així fa el càlcul mode 256
        HMMM(stamp_x, stamp_y, desti_x, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y -= 1;
      multipleVuit = 1;
    }
    SetScrollV(pos_scroll_y);

    // Copiem cartell visible a la nova posició (abaix una línia)
    // Si el càlcul de la y fa que estigui a la pàgina anterior l'he de
    // controlar per dividir entre 2
    if (pos_scroll_y<255 && pos_scroll_y>255-11) {
      // Comencem final de tot pàgina visible a la línia 256-pos_scroll_y
      debugar = 11;
      // 1 - Copiem línia de buffer del final a sota del cartell
      HMMM(255-94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255 - multipleVuit, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 12 - 256 + pos_scroll_y - multipleVuit, 94, 1 + multipleVuit);
      // 2 - Baixem tot el buffer
      fastVDP.sx = 255-94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 254 - multipleVuit;
      fastVDP.dx = 255-94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255;
      fastVDP.nx = 94;
      fastVDP.ny = 11 - multipleVuit;
      fastVDP.col = 0;
      fastVDP.param =  opUP;
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);
      
      // 3 - Copiem línia dalt cartell al buffer
      // La part de dalt del buffer no s'està copiant bé, per això no copia bé les dues línies
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y - multipleVuit, 255-94,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 94, 1 + multipleVuit);
      // 4 - Copiem tot el cartell amunt
      // Primer la part que ja es troba al final de la pàgina, que seria la part
      // de dalt del cartell
      /********************************** Vaig per aquí, comentar les parts i anar d'una en una a veure què fan */
      if (multipleVuit ==0) {
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 1 - multipleVuit, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 94,
           255 - pos_scroll_y - multipleVuit);
      // Segon la línia de dalt de tot a final de la pàgina
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2, 80 - 50*multipleVuit,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 255 - multipleVuit, 94, 1 + multipleVuit);
      // Tercer la part de dalt de la pàgina, la part baixa del cartell, la pugem una línia
      if (11 - 256 + pos_scroll_y > 0) {
        HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 1 - multipleVuit,
             80 + 30 * multipleVuit, OFFSET_COORDENADAY_PAGINA_ACTIVA_2, 94,
             11 - 256 + pos_scroll_y + multipleVuit);
      }
      }
      else {
        HMMM(80,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 1 +
                 multipleVuit,
             80,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y , 94,
             255 - pos_scroll_y - multipleVuit);
        // Segon la línia de dalt de tot a final de la pàgina
        HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2,
             80-40,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 255 - multipleVuit, 94,
             1 + multipleVuit);
        // Tercer la part de dalt de la pàgina, la part baixa del cartell, la
        // pugem una línia
        /* if (11 - 256 + pos_scroll_y > 0) { */
        /*   HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2+1, */
        /*        80 -40, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 - multipleVuit, 94, */
        /*        11 - 256 + pos_scroll_y + multipleVuit); */
        /* } */
      }
      // 5 - Copiem la línia final del cartell a la pantalla de joc
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 253 - multipleVuit, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 +11 -256 + pos_scroll_y - multipleVuit, 94, 1 + multipleVuit);

    } else if (pos_scroll_y == 255) {
      debugar = 22;
      // 1 - Copiem línia de buffer del final a sota del cartell
      // Com que és el moment que he fet doble scroll, n'he de copiar 2
      HMMM(255-94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 254, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 11, 94, 2);
      // 2 - Baixem tot el buffer
      // El baixem dos llocs i per tant copiem una línia menys, ny és 10 en comptes d'11
      fastVDP.sx = 255-94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 253;
      fastVDP.dx = 255-94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255;
      fastVDP.nx = 94;
      fastVDP.ny = 10;
      fastVDP.col = 0;
      fastVDP.param =  opUP;
      // https://konamiman.github.io/MSX2-Technical-Handbook/md/Chapter4b.html#651-hmmc-cpu---vram-high-speed-transfer El registre 45
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);
      
      // 3 - Copiem línia dalt cartell al buffer
      // També dos llocs, ja que fem scroll doble
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y-1, 255-94,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 94, 2);
      // 4 - Copiem tot el cartell amunt
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2+1, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 94, 1);
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 2, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2, 94, 10);
      // 5 - Copiem la línia final del cartell a la pantalla de joc
      // Com que és el moment que he fet el doble scroll, n'he de copiar 2
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 252, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 9, 94, 2);
    } else if (pos_scroll_y == 244) {
      debugar = 44;
      // 1 - Copiem línia de buffer del final a sota del cartell
      HMMM(255 - 94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2, 94, 1);
      // 2 - Baixem tot el buffer
      fastVDP.sx = 255-94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 254;
      fastVDP.dx = 255-94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255;
      fastVDP.nx = 94;
      fastVDP.ny = 11;
      fastVDP.col = 0;
      fastVDP.param =  opUP;
      // https://konamiman.github.io/MSX2-Technical-Handbook/md/Chapter4b.html#651-hmmc-cpu---vram-high-speed-transfer El registre 45
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);
      
      // 3 - Copiem línia dalt cartell al buffer
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 255-94,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 94, 1);
      // 4 - Copiem tot el cartell amunt
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 1, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 94, 11);
      // 5 - Copiem la línia final del cartell a la pantalla de joc
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 253, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 255, 94, 1);
    } else {
      // 1 - Copiem línia de buffer del final a sota del cartell
      // Quan arribem al 256 ha d'anar a la 0
        HMMM(255 - 94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255, 80,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 12, 94, 1);
      // 2 - Baixem tot el buffer
      fastVDP.sx = 255-94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 254; // L'origen ara és avall, pel sentit d'up que li donc al copiar
      fastVDP.dx = 255-94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255;
      fastVDP.nx = 94;
      fastVDP.ny = 11;
      fastVDP.col = 0;
      fastVDP.param =  opUP;
      // https://konamiman.github.io/MSX2-Technical-Handbook/md/Chapter4b.html#651-hmmc-cpu---vram-high-speed-transfer El registre 45
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);

      // 3 - Copiem línia dalt cartell al buffer
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 255-94,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 94, 1);
      // 4 - Copiem tot el cartell amunt
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 1,
           80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 94, 12);
      // 5 - Copiem la línia final del cartell a la pantalla de joc
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 253, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 11, 94, 1);
    }
  }
  if(map_tile_y == 0 && fer_scroll_vertical==1){
    fer_scroll_vertical = 0;
    pos_scroll_y -= 1;
  }
}

void scroll_avall() {
  multipleVuit = 0;
  if (map_tile_y < NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER - 1){
    fer_scroll_vertical = 1;
  }

  if(fer_scroll_vertical==1){
    pos_scroll_y += 1;

    if ((pos_scroll_y & 7) == 0) {
      map_tile_y += 1;
      desti_y = pos_scroll_y + 216;
      for (int n = 0; n < NOMBRE_RAJOLES_HOR_ORIGEN_PATRONS; n++) {
        obtenir_coordenades_rajola(n + map_tile_x,
                                   map_tile_y + NOMBRE_RAJOLES_PANTALLA_VER - 1);
        desti_x = (n + (pos_scroll_x>>3)) * 8;
        HMMM(stamp_x, stamp_y, desti_x, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + desti_y, 8, 8);
      }
      pos_scroll_y += 1;
      // Copm que he afegit aquest increment perquè no es tallessin els scrolls. També l'hauré de tenir en compte en fer l'extra
      multipleVuit = 1;
    }
    SetScrollV(pos_scroll_y);

    // Copiem cartell visible a la nova posició (abaix una línia)
    // Si el càlcul de la y fa que estigui a la pàgina anterior l'he de
    // controlar per dividir entre 2
    if (pos_scroll_y<=255 && pos_scroll_y>=255-11) {
      // Comencem final de tot pàgina visible a la línia 256-pos_scroll_y
      debugar = 111;
      // 1 - Copiem línia de buffer de dalt a dalt el cartell
      // !!!!!!! Faltarà afegir tot el pos_scroll_x a les coordenades dels scrolls verticals
      HMMM(255-94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 80 + pos_scroll_x ,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y-1 , 94, 1);
      // 2 - Pugem tot el buffer
      fastVDP.sx = 255-94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 245;
      fastVDP.dx = 255-94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244; // També hauré de tenir en compte l'scroll
      fastVDP.nx = 94;
      fastVDP.ny = 11;
      fastVDP.col = 0;
      fastVDP.param = opDOWN;
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);
      
      // 3 - Copiem línia baix del cartell a baix del buffer
      HMMM(80, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 12 + pos_scroll_y  - 256, 255-94,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255, 94, 1);

      // 4 - Copiem tot el cartell avall
      // Ara ha de ser al revés que quan anàvem cap amunt.
      // Primer, la part que es troba a principi de la pàgina ha de baixar un lloc
      if (pos_scroll_y > 245) {
        fastVDP.sx = 80;
        fastVDP.sy =
            OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 10 - 256 + pos_scroll_y;
        fastVDP.dx = 80;
        fastVDP.dy =
            OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 11 - 256 + pos_scroll_y;
        fastVDP.nx = 94;
        fastVDP.ny = 11 - 256 + pos_scroll_y;
        fastVDP.col = 0;
        fastVDP.param = opUP;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
      }
      // Segon la línia de final de la pàgina a dalt de tot
      // Una reducció fàcil de codi són aquells paràmetres que sempre són iguals, no cal que els vagi definint cada vegada
      fastVDP.sx = 80;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 255;
      fastVDP.dx = 80;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2;
      fastVDP.nx = 94;
      fastVDP.ny = 1;
      fastVDP.col = 0;
      fastVDP.param = opUP;
      fastVDP.cmd = opHMMM;
      FastVDPcopy(&fastVDP);

      // Tercer la part que ja es troba al final de la pàgina ha de baixar una línia
      // de dalt del cartell
      if (pos_scroll_y != 255) {
        fastVDP.sx = 80;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 254;
        fastVDP.dx = 80;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 255;
        fastVDP.nx = 94;
        fastVDP.ny = 255 - pos_scroll_y;
        fastVDP.col = 0;
        fastVDP.param = opUP;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
      }

      // Això era del 3r pas
      // Sembla que ho estigui fent bé, però que no està omplint el buffer
      if (pos_scroll_y == 255) {
        fastVDP.col = 0;
      }

      // 5 - Copiem la línia prinicpi del cartell a la pantalla de joc
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 94, 1);
    } else {
      debugar = 133;

      // 1 - Copiem línia de buffer de dalt a dalt el cartell
      HMMM(255 - 94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244,
           80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y - 1 - (multipleVuit<<1), // Multipliquem *2 ja que s'ha de restar dos quan és multipleVuit
           94, 1 + multipleVuit);
      // 2 - Pugem tot el buffer
      fastVDP.sx = 255 - 94;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 245;
      fastVDP.dx = 255 - 94;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
      fastVDP.nx = 94;
      fastVDP.ny = 11;
      fastVDP.col = 0;
      fastVDP.param = opDOWN;
      fastVDP.cmd = opYMMM;
      FastVDPcopy(&fastVDP);

      // 3 - Copiem línia baix del cartell a baix del buffer
      HMMM(80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + 12 + pos_scroll_y,
           255 - 94, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255, 94, 1);
      // 4 - Copiem tot el cartell avall
      fastVDP.sx = 80;
      fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 11 -
        1 - multipleVuit; // Només he de restar el valor 1 en el multipleVuit
      fastVDP.dx = 80;
      fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y + 12 -
        1;
      fastVDP.nx = 94;
      fastVDP.ny = 11;
      fastVDP.col = 0;
      fastVDP.param = opUP;
      fastVDP.cmd = opHMMM;
      FastVDPcopy(&fastVDP);

      // 5 - Copiem la línia principi del cartell a la pantalla de joc
      HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242, 80,
           OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y,
           94, 1);
        

    }
  }
  if ((map_tile_y == NOMBRE_RAJOLES_VER - NOMBRE_RAJOLES_PANTALLA_VER + 1) && fer_scroll_vertical==1) {
    // He de sumar a NOMBRE_RAJOLES_PANTALLA_VER perquè. També hem de tenir en compte que queda a mitges. S'haurà de pintar el bordó
    fer_scroll_vertical = 0;
    pos_scroll_y += 1;
  }
}

void scroll_esq() {
  if (map_tile_x>3) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1) {
    // Hauré de controlar l'offset inicial per restar -2 o -4. Si el pos_scroll
    // és senar ho fa malament
    if ((pos_scroll_x & 1) == 0) {
      if (pos_scroll_x>0) {
        // 1 - Copiem línia de la dreta del buffer a la dreta del cartell
        HMMM(252, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244,
             80 + 94 + pos_scroll_x -2 ,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 2, 12);
        // 2 - Desplacem el buffer a la dreta
        fastVDP.sx = 253; // 255 - 94 + 1;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.dx = 255; // 255 - 94;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.nx = 94;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opLEFT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 3 - Copiem línia esquerra del cartell a l'esquerra del buffer
        HMMM(80 + pos_scroll_x - 2,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 160,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255 - 11, 2, 12);
        // 4 - Copiem tot el cartell a l'esquerra
        // Quant he recorregut a l'esquerra
        char scroll_cast = (char) (80 + pos_scroll_x); // Mirar si així és correcte, perquè no ho fa amb el fastVDP.sx, mirar la diferència del codi generat
        debugar = scroll_cast;
        fastVDP.sx = scroll_cast;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
        fastVDP.dx = fastVDP.sx - 2;
        fastVDP.dy = fastVDP.sy;
        fastVDP.nx = 94 - 2;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opRIGHT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 5 - Copiem línia dreta del cartell a la pantalla de joc
        HMMM(6 + 94 - 2, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242,
             80 + 94 + pos_scroll_x -4,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 2, 12);
      }
    }
    pos_scroll_x -= 1;
    if ((pos_scroll_x & 7) == 0) {
      // Abans restava aquest pos_scroll abans de l'if, però m'alterava els cops que restava -2 ó -4, ja que el primer desplaçament no el feia. Per exemple, pos_scroll era 2C, restava un i no entrava, però ja havia desplaçat la pantalla i quan sí entrava al 2A ja s'havia desplaçat 2 cops i em desquadrava
      map_tile_x -= 1; // És un char, no arriba mai a ser més petit que 0
      // Falta la segona part que queda tallada entre les dues pàgines
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256
      desti_y = (pos_scroll_y) >> 3;
      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y; m++) {
        obtenir_coordenades_rajola(map_tile_x, m + map_tile_y);
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + (m+desti_y) * 8, 8, 8);
      }
      for (int m = NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y;
           m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL; m++) {
        obtenir_coordenades_rajola(map_tile_x , m + map_tile_y);
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 +
                 (m - NOMBRE_RAJOLES_PANTALLA_VER_SCROLL + desti_y) * 8, 8, 8);
      }
      // Falla aquesta part
      pos_scroll_x -= 1;
    }
    SetScrollH(pos_scroll_x);

  }
  if (map_tile_x == 3 && fer_scroll_lateral == 1) {
    // Per què ha de ser 3? Pot ser menor
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x -=1; // Faltava aquest scroll per poder mantenir la lògica de les caselles
  }
}

void scroll_dreta() {
  if (map_tile_x < NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR - 1) {
    fer_scroll_lateral = 1;
  }
  if (fer_scroll_lateral==1){
    pos_scroll_x += 1;
    if ((pos_scroll_x & 7) == 0) {
      map_tile_x += 1; 
      desti_x = pos_scroll_x - 8; // D'aquesta forma fa l'operació com a char, en mòdul 256
      desti_y = (pos_scroll_y)>>3; 
      // S'ha de copiar al mateix lloc que l'scroll esquerra ja que comparteixen els mateixos 8 pixels d'amplada
      for (int m = 0; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y; m++) {
        obtenir_coordenades_rajola(map_tile_x + 31, m + map_tile_y); // L'offset 31 és per la part que toca de la dreta del mapa general, map_tile_x té només la part esquerra del mapa general
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + (m + desti_y) * 8, 8, 8); 
      }
      for (int m = NOMBRE_RAJOLES_PANTALLA_VER_SCROLL - desti_y; m < NOMBRE_RAJOLES_PANTALLA_VER_SCROLL; m++) {
        obtenir_coordenades_rajola( map_tile_x + 31, m + map_tile_y);
        HMMM(stamp_x, stamp_y, desti_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + (m - NOMBRE_RAJOLES_PANTALLA_VER_SCROLL + desti_y) * 8,
             8, 8);
      }
      pos_scroll_x += 1;
    }
    SetScrollH(pos_scroll_x);
    if ((pos_scroll_x & 1) == 0) {
      // Com que és múltiple de dos només ho farem en els desplacems parells
      if (pos_scroll_x <= 83) {
        // 1 - Copiem línia del buffer a l'esquerra del cartell
        // Per limitacions de hardware no puc copiar una línea, ha de ser
        // múltiple de 2
        HMMM(160, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244,
             80 + pos_scroll_x - 2,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 2, 12);
        // 2 - Desplacem el buffer a l'esquerra
        debugar = 321;
        fastVDP.sx = 162; // 255 - 94 + 1;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.dx = 160; // 255 - 94;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.nx = 94;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opRIGHT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 3 - Copiem línia dreta del cartell a la dreta del buffer
        HMMM(80 + pos_scroll_x + 94 - 2,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 252,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255 - 11, 2, 12);
        // 4 - Copiem tot el cartell a la dreta
        fastVDP.sx = 80 + pos_scroll_x + 92 - 2;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y ;
        fastVDP.dx = 80 + pos_scroll_x + 92;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y ;
        fastVDP.nx = 94 - 2;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opLEFT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 5 - Copiem línia esquerra del cartell a la pantalla de joc
        HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242,
             80 + pos_scroll_x, OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y,
             2, 12);
      } else {
        // Hem de començar a dividir els copies ja que està al límit de la pantalla
        // 1 - Copiem línia del buffer a l'esquerra del cartell 
        HMMM(160, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244, 80 + pos_scroll_x - 2,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 2, 12);
        // 2 - Desplacem el buffer a l'esquerra
        debugar = 321;
        fastVDP.sx = 162; // 255 - 94 + 1;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.dx = 160; // 255 - 94;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 244;
        fastVDP.nx = 94;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opRIGHT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 3 - Copiem línia dreta del cartell a la dreta del buffer
        // Copiem a la coordenada X 0, ja que és a l'origen. Resto tot l'offset que teníem
        HMMM(pos_scroll_x - 83,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 252,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 255 - 11, 2, 12);
        // 4 - Copiem tot el cartell a la dreta
        // La part que ja havíem enganxat només si ja l'he començada a enganxar
        // Vaig per aquí !!!!!!!!!!!!! Calcular les coordenades
        if (pos_scroll_x > 85) {
          fastVDP.sx = pos_scroll_x - 83 - 2;
          fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
          fastVDP.dx = pos_scroll_x - 83;
          fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
          fastVDP.nx = pos_scroll_x - 83;
          fastVDP.ny = 12;
          fastVDP.col = 0;
          fastVDP.param = opLEFT;
          fastVDP.cmd = opHMMM;
          FastVDPcopy(&fastVDP);
        }
        // La nova part que creua la pantalla
        fastVDP.sx = 254;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
        fastVDP.dx = 0;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
        fastVDP.nx = 2;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opRIGHT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // La resta la desplacem
        int calcul_queda_restant = 94 - pos_scroll_x+83;
        // Feia que es copiés tota la pantalla si feia el càlcul directament a les .nx
        debugar = calcul_queda_restant;
        fastVDP.sx = 253;
        fastVDP.sy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
        fastVDP.dx = 255;
        fastVDP.dy = OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y;
        fastVDP.nx = calcul_queda_restant;
        fastVDP.ny = 12;
        fastVDP.col = 0;
        fastVDP.param = opLEFT;
        fastVDP.cmd = opHMMM;
        FastVDPcopy(&fastVDP);
        // 5 - Copiem línia esquerra del cartell a la pantalla de joc
        HMMM(6, OFFSET_COORDENADAY_PAGINA_ACTIVA_1 + 242, 80 + pos_scroll_x,
             OFFSET_COORDENADAY_PAGINA_ACTIVA_2 + pos_scroll_y, 2, 12);
      }
    }
  }
  if (map_tile_x == (NOMBRE_RAJOLES_HOR - NOMBRE_RAJOLES_PANTALLA_HOR -1) && fer_scroll_lateral == 1) {
    // Ha fet la volta del char
    fer_scroll_lateral = 0;
    pos_scroll_x +=1 ; 
  }
}

char stick;
char space;
char x,y;
char strx[6], stry[6];
char es_casellaParet(int nombre_casella){
  if (map1[nombre_casella]<3) {
      return(1);
  }  else if (map1[nombre_casella]>=32 && map1[nombre_casella]<=34 ) {
    return(1);
  } else if (map1[nombre_casella] >= 64 && map1[nombre_casella] <= 66) {
    return (1);
  } else if (map1[nombre_casella] >= 96 && map1[nombre_casella] <= 98) {
    return (1);
  } else {
    return(0);
  }
}

char tile_esq;
char tile_amunt;
int casella_1;
int casella_2;
void calculem_tiles(){
  // Investiguem caselles a on estem
  tile_esq = (x + (pos_scroll_x & 7)) >> 3;
  tile_amunt = (y + (pos_scroll_y & 7)) >> 3;
}
char moviment_amunt(){
  char mogut = 1;
  y = (y - 1);
  calculem_tiles();
  // Mirem si la casella d'abaix a l'esquerra és una casella (tile) de paret
  // He de mirar tant el de la meva esquerra tile_esq, més el de la meva dreta
  // que és la part del cos de la dreta tile_esq+1
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y + 1;
    mogut = 0;
  }
  if (y < 14) {
    y = y + 1;
    mogut = 0;
    scroll_amunt();
  }
  return(mogut);
}
char moviment_avall(){
  char mogut = 1;
  y = (y + 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  casella_2 = casella_1 - 1;
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    y = y - 1;
    mogut = 0;
  }
  if (y > 206 - 8) {
    y = y - 1;
    mogut = 0;
    scroll_avall();
  }
  return(mogut);
}
char moviment_dreta(){
  char mogut = 1;
  x = (x + 1);
  calculem_tiles();
  casella_1 =
      tile_esq + map_tile_x + NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x - 1;
    mogut = 0;
  }
  if (x > 241) {
    // Els primers estan amagats
    x = x - 1;
    mogut = 0;
    scroll_dreta();
  }
  return(mogut);
}
char moviment_esquerra(){
  char mogut = 1;
  x = (x - 1);
  calculem_tiles();
  casella_1 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y);
  casella_2 = tile_esq + map_tile_x - 1 +
              NOMBRE_RAJOLES_HOR * (tile_amunt + map_tile_y + 1);
  if (es_casellaParet(casella_1) == 1 || es_casellaParet(casella_2) == 1) {
    x = x + 1;
    mogut = 0;
  }
  if (x < 14) {
    // Els primers estan amagats
    x = x + 1;
    mogut = 0;
    scroll_esq();
  }
  return(mogut);
}
void moviment_sprite(){
  char mogut_lateral = 0;
  char mogut_vertical = 0;
  while (Inkey()!=27)
  {
    if (processar_moviment == 1) {
      stick = JoystickRead(0);
      space = TriggerRead(0);

      if (stick == 1) {
        // Amunt
        mogut_vertical = moviment_amunt();
      }
      else if (stick == 2) {
        // Amunt dreta
        mogut_lateral = moviment_dreta();
        mogut_vertical = moviment_amunt();
      } else if (stick == 3) {
        // Dreta
        mogut_lateral = moviment_dreta();
      } else if (stick == 4) {
        // Avall Dreta
        mogut_lateral = moviment_dreta();
        mogut_vertical = moviment_avall();
      } else if (stick == 5) {
        // Avall
        mogut_vertical = moviment_avall();
      } else if (stick == 6) {
        // Avall Esquerra
        mogut_lateral = moviment_esquerra();
        mogut_vertical = moviment_avall();
      } else if (stick == 7) {
        // Esquerra
        mogut_lateral = moviment_esquerra();
      } else if (stick == 8) {
        // Amunt Esquerra
        mogut_lateral = moviment_esquerra();
        mogut_vertical = moviment_amunt();
      }
      PutSprite(0, 0, x, y+pos_scroll_y, 4); // Sumem el pos_scroll_y, ja que hi ha un offset cada cop que fem scroll
      if( mogut_lateral == 1 || mogut_vertical == 1) {
        // debugar = 100;
        // Els següents if no els agafava bé amb el valor de if-else
        unsigned char hist_mov = historic_moviments[punter_historic];
        if (hist_mov == 1) {
          // Amunt
          y2 = y2 - 1;
        }
        if (hist_mov == 2) {
          // Amunt dreta
          x2 += 1;
          y2 -= 1;
        }
        if (hist_mov == 3) {
          // Dreta
          x2 += 1;
        }
        if (hist_mov == 4) {
          // Avall Dreta
          y2 += 1;
          x2 += 1;
        }
        if (hist_mov == 5) {
          // Avall
          y2 += 1;
        }
        if (hist_mov == 6) {
          // Avall Esquerra
          x2 -= 1;
          y2 += 1;
        }
        if (hist_mov == 7) {
          // Esquerra
          x2 -= 1;
        }
        if (hist_mov == 8) {
          // Amunt Esquerra
          x2 -= 1;
          y2 -= 1;
        }

        historic_moviments[punter_historic] = stick;
        if (mogut_lateral == 0) {
          if (stick ==2 || stick == 8 ) {
            historic_moviments[punter_historic] = 1;
          } else if (stick == 6 || stick == 4) {
            historic_moviments[punter_historic] = 5;
          }
        } else if (mogut_vertical == 0) {
          if (stick == 2 || stick == 4) {
            historic_moviments[punter_historic] = 3;
          } else if (stick == 6 || stick == 8) {
            historic_moviments[punter_historic] = 7;
          }
        }
        punter_historic = (punter_historic + 1) & 15;
      }
      PutSprite(1, 1, x2, y2+pos_scroll_y, 4);
      mogut_vertical = 0;
      mogut_lateral = 0;
      processar_moviment = 0;
    }
  }
}

void main(){
  Screen(5);
  SetDisplayPage(2);
  map_tile_x = 12;
  map_tile_y = 14;
  // Canviem el color 15 perquè sigui blanc i el text
  SetColorPalette(15 , 7, 7, 7);
  copsVsync = 0;
  processar_moviment=0;
  pos_scroll_x = 1;
  pos_scroll_y = 1; // L'inicialitzem amb 1 perquè així si vaig amunt també el detecta com a 0. 
  fer_scroll_lateral = 1;
  fer_scroll_vertical = 1;
  SetScrollMask(1);
  SetScrollH(pos_scroll_x); // Comencem a la mateixa posició que el que hem après de scrpro.c
  SetScrollV(pos_scroll_y);
  init_pantalla_joc();

  InitializeMyInterruptHandler((int)main_loop, 1);
  // Posem sprite al 50,50
  x=28; y=40; x2=12; y2=40;
  punter_historic = 0;
  PutSprite(0, 0, x, y, 4);
  moviment_sprite();
  EndMyInterruptHandler();
  Screen(0);
  RestorePalette();
  Locate(0, 0);
  printf("Pos sprite: %d, %d; 2: %d, %d",x,y, x2,y2);
  Locate(0,1);

  printf("Tiles: %d, %d; %d-%d; %d-%d", tile_esq, tile_amunt,
         casella_1, casella_2,
         es_casellaParet(casella_1), es_casellaParet(casella_2));
  Locate(0,2);
  printf("scrl_x:%d;scrl_y;%d;", pos_scroll_x, pos_scroll_y);
  Locate(0, 3);
  printf("mptle_x:%d;frscrlat:%d;", map_tile_x, fer_scroll_lateral);
  Locate(0, 4);
  printf("mptle_y:%d;frscrver:%d;", map_tile_y, fer_scroll_vertical);
  Locate(0, 5);
  printf("punter_historic: %d;\n\r", punter_historic);
  printf("hist_mov: %d, %d, %d, %d  ,  %d, %d, %d, %d\n\r",
         historic_moviments[0], historic_moviments[1], historic_moviments[2], historic_moviments[3],
         historic_moviments[4], historic_moviments[5], historic_moviments[6], historic_moviments[7]);
  printf("hist_mov2: %d, %d, %d, %d  ,  %d, %d, %d, %d\n\r",
         historic_moviments[8], historic_moviments[9], historic_moviments[10],
         historic_moviments[11], historic_moviments[12], historic_moviments[13],
         historic_moviments[14], historic_moviments[15]);
  printf("debugar: %d; multVuit: %d", debugar, multipleVuit);
  Exit(0);
}
