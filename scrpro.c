/* L'scroll no el feia gaire bé, no l'he acabat d'entendre. El faig aquí de froma simplificada per fer proves
 */

#include "../fusion-c/include/msx_fusion.h"
#include "../fusion-c/include/vdp_graph2.h"
#include "../fusion-c/include/vdp_sprites.h"
#include <string.h>  // necessària pel memset

static FCB file; // Initialisatio de la structure pour le systeme de fichiers

#define BUFFER_SIZE_SC5 2560

unsigned char LDbuffer[BUFFER_SIZE_SC5];
char mypalette[16 * 4];
char map_tile_x;
char map_tile_y;


static const unsigned char sprite[] = {
  0b00111000,
  0b01000100,
  0b00101000,
  0b00010000,
  0b00111000,
  0b01111100,
  0b01111110,
  0b11111111
};


void FT_SetName(FCB *p_fcb, const char *p_name) // Routine servant à vérifier le
                                                // format du nom de fichier
{
  char i, j;
  memset(p_fcb, 0, sizeof(FCB));
  for (i = 0; i < 11; i++) {
    p_fcb->name[i] = ' ';
  }
  for (i = 0; (i < 8) && (p_name[i] != 0) && (p_name[i] != '.'); i++) {
    p_fcb->name[i] = p_name[i];
  }
  if (p_name[i] == '.') {
    i++;
    for (j = 0; (j < 3) && (p_name[i + j] != 0) && (p_name[i + j] != '.');
         j++) {
      p_fcb->ext[j] = p_name[i + j];
    }
  }
}

void FT_errorHandler(char n, char *name) // Gère les erreurs
{
  Screen(0);
  SetColors(15,6,6);
  switch (n)
  {
      case 1:
          Print("\n\rFAILED: fcb_open(): ");
          Print(name);
      break;
 
      case 2:
          Print("\n\rFAILED: fcb_close():");
          Print(name);
      break;  
 
      case 3:
          Print("\n\rStop Kidding, run me on MSX2 !");
      break; 
  }
Exit(0);
}

int FT_LoadSc5Image(char *file_name, unsigned int start_Y, char *buffer, unsigned int amplada_linia, unsigned int tamany_buffer)        // Charge les données d'un fichiers
    {
      // El tamany de tamany_buffer és el nombre de bytes que fan les 20 linies. En screen 5 és 2560, però en screen 7 és 2560*2
        int rd=2560;

        FT_SetName( &file, file_name );
        if(fcb_open( &file ) != FCB_SUCCESS) 
        {
              FT_errorHandler(1, file_name);
              return (0);
        }

        fcb_read( &file, buffer, 7 );  // Skip 7 first bytes of the file
        // La imatge creada també amb el Viewer té els 7 primers bytes com
        // s'indica a la secció de binary files de
        // https://www.msx.org/wiki/MSX-BASIC_file_formats
        while (rd!=0)
        {
             rd = fcb_read( &file, buffer, tamany_buffer );  // Read 20 lines of image data (128bytes per line in screen5)
             HMMC(buffer, 0,start_Y,amplada_linia,20 ); // Move the buffer to VRAM. 
             start_Y=start_Y+20;
         }

return(1);
}
int FT_LoadPalette(char *file_name, char *buffer, char *mypalette) 
{

  // El vector és de 24 bytes, cadascú té doble valor que fan 48=16*3 ordenats com RG BR GB RG BR GB
  unsigned char paleta_flatten[48];

  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 7); // Skip 7 first bytes of the file
  fcb_read(&file, buffer, 24); 
  for(int k=0; k<24; k++){
    paleta_flatten[2*k] = buffer[k]>>4 & 0x07;
    paleta_flatten[2*k+1] = buffer[k] & 0x07;
  }
  for(int k=0; k<16; k++) {
    mypalette[4 * k] = k;
    mypalette[4 * k + 1] = paleta_flatten[3 * k]; //red
    mypalette[4 * k + 2] = paleta_flatten[3 * k + 1]; // green
    mypalette[4 * k + 3] = paleta_flatten[3 * k + 2]; // blue
  }
  SetPalette((Palette *)mypalette);

  return (1);
}

int FT_LoadPalette_MSXViewer(char *file_name, char *buffer, char *mypalette) {
  // Tal i com s'indica a l'exemple del manual
  // http://marmsx.msxall.com/msxvw/msxvw5/download/msxvw5_7_man.pdf la paleta és un binari. Si desensamblem el codi
  FT_SetName(&file, file_name);
  if (fcb_open(&file) != FCB_SUCCESS) {
    FT_errorHandler(1, file_name);
    return (0);
  }

  fcb_read(&file, buffer, 0x29); 
  for (int k=0;k<16;k++){
    mypalette[k*4] = k;
    fcb_read(&file, &mypalette[k*4+1], 3); // Els podem llegir directament. Cada byte és el color
  }
  SetPalette((Palette *)mypalette);

  return (1);
}


void init_pantalla_joc() {
  // Carreguem la imatge dels patrons1
  FT_LoadSc5Image("UsasPatr.sc5", 256, LDbuffer, 512,
                  BUFFER_SIZE_SC5); // Carreguem la imatge
  FT_LoadPalette("UsasPatr.pl5", LDbuffer, mypalette);
  SetPalette((Palette *)mypalette);


  // SpriteDouble();
  SpriteSmall();
  SetSpritePattern(0, sprite, 8);
  char colorSprites[] = {1, 9, 10, 1, 9, 10, 7, 13};
  SetSpriteColors(0, colorSprites);
}

int pos_scroll_x;
int pos_scroll_y;


void main(){
  Screen(5);
  SetDisplayPage(1);
  init_pantalla_joc();
  // Canviem el color 15 perquè sigui blanc i el text
  SetColorPalette(15 , 7, 7, 7);
  pos_scroll_x = 0;
  pos_scroll_y = 0;
  SetScrollMask(1);
  PutSprite(0, 0, 50, 50, 4);

  SetScrollH(0); // Inicialitzem el valor de l'scroll per deixar-lo ben posat i que es compleixin les restriccions del 249
  WaitKey();
  SetScrollH(1); // Avancem en 1 i així ho podem fer copiant en els parells per les limitacions del HW
  WaitKey();
  // La part oculata comença al 249 i acaba al 0 que també està ocult. Hem de
  // tenir en compte que hi ha la limitació de 2 pixels per hardware 9938
  HMMM(48, 256, 250, 256, 6,
       256); // Comença al 250, és el que es comença mostrant
  WaitKey();
  // Si en copio 8 en comptes de 6, no sobreescriu els 8 primers, es perden
  HMMM(54, 256, 0, 256, 2,
       256); // Comença al 250, és el que es comença mostrant
  WaitKey();

  // La part que oculta és de 8px però va del 249 al 0 (una mica estrany)
  // El problema és que quan mires els registres està indicant el -7 perquè és el valor que representen els 3LSB quan valen 0
  SetScrollH(2);
  WaitKey();
  SetScrollH(4);
  WaitKey();
  SetScrollH(9);
  WaitKey();

  HMMM(56, 256, 2, 256, 8, 256);
  WaitKey();
  SetScrollH(10);
  WaitKey();
  SetScrollH(17);
  WaitKey();

  HMMM(64, 256, 10, 256, 8, 256);
  WaitKey();
  SetScrollH(18);
  WaitKey();
  SetScrollH(25);
  WaitKey();

  HMMM(72, 256, 18, 256, 8, 256);
  WaitKey();
  SetScrollH(26);
  WaitKey();
  SetScrollH(33);
  WaitKey();

  // Ara anirem cap a la dreta
  HMMM(120,256,26, 256, 8, 256);
  WaitKey();
  SetScrollH(30);
  WaitKey();
  SetScrollH(25);
  WaitKey();

  HMMM(112, 256, 18, 256, 8, 256);
  WaitKey();
  SetScrollH(17);
  WaitKey();

  HMMM(104, 256, 10, 256, 8, 256);
  WaitKey();
  SetScrollH(9);
  WaitKey();

  Screen(0);
  RestorePalette();
  Exit(0);
}
